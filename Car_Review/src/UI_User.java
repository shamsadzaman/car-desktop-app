import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class UI_User extends JFrame {

	// shows information about insert/update/delete operation
	// e.g. update successful/couldn't create user
	private JLabel lblMessage;
	private String lblInfoSuccess;
	private String lblInfoFailed;

	private JLabel lblUserName;
	private JTextField txtUserName;
	private JLabel infoUserName;

	private JLabel lblEmail;
	private JTextField txtEmail;
	private JLabel infoEmail;

	private JLabel lblAge;
	private JTextField txtAge;

	private JLabel lblGender;
	private JTextField txtGender;
	private JLabel infoGender;

	private JLabel lblCity;
	private JTextField txtCity;

	private JLabel lblState;
	private JTextField txtState;
	private JLabel infoState;

	private JButton btnReset;
	private JButton btnUser;
	private JButton btnMemByUsername;
	private JButton btnMemByEmail;
	private JButton btnMainMenu;

	public static enum WINDOW_MODE {
		CREATE, UPDATE, DELETE
	};

	private WINDOW_MODE currentMode;

	private String lblInfoUnique;
	private String lblInfoGender;
	private String lblInfoState;

	private User user;

	public UI_User(WINDOW_MODE wMode) {
		// TODO Auto-generated constructor stub
		super(wMode.toString() + " MEMBER");
		currentMode = wMode;

		lblInfoSuccess = "Success";
		lblInfoFailed = "Failed";

		lblInfoUnique = "must be unique";
		lblInfoGender = "male/female";
		lblInfoState = "two letter code of \rstates (e.g. OH, MI, NY)";

		// if(user == null)
		// user = new User();

		CreateWindow();
	} // end of constructor

	private void CreateWindow() {

		setLayout(new GridLayout(8, 4));
		setLocation(500, 300);
		setSize(500, 300);

		lblMessage = new JLabel();

		lblUserName = new JLabel("*Username: ");
		txtUserName = new JTextField(20);
		infoUserName = new JLabel(lblInfoUnique);

		lblEmail = new JLabel("*Email: ");
		txtEmail = new JTextField(20);
		infoEmail = new JLabel(lblInfoUnique);

		lblAge = new JLabel("Age: ");
		txtAge = new JTextField(20);

		lblGender = new JLabel("Gender: ");
		txtGender = new JTextField(20);
		infoGender = new JLabel(lblInfoGender);

		lblCity = new JLabel("City: ");
		txtCity = new JTextField(20);

		lblState = new JLabel("State: ");
		txtState = new JTextField(20);
		infoState = new JLabel(lblInfoState);

		String strBtnUser = "Create Member"; // default value

		if (currentMode == WINDOW_MODE.UPDATE) {
			strBtnUser = "Update Member";
		} else if (currentMode == WINDOW_MODE.DELETE) {
			strBtnUser = "Delete Member";
		}

		btnReset = new JButton("Reset");
		btnUser = new JButton(strBtnUser);
		btnMemByEmail = new JButton("Find Member");
		btnMemByUsername = new JButton("Find Member");
		btnMainMenu = new JButton("Main Menu");

		add(lblMessage);
		add(new JLabel());
		add(new JLabel("instruction"));
		add(new JLabel());

		add(lblUserName);
		add(txtUserName);
		if (currentMode != WINDOW_MODE.CREATE) // add find member data button
												// for update & delete mode
			add(btnMemByUsername);
		add(infoUserName);
		if (currentMode == WINDOW_MODE.CREATE) // fill in empty column for
												// create mode
			add(new JLabel());

		add(lblEmail);
		add(txtEmail);
		if (currentMode != WINDOW_MODE.CREATE) // add find member data button
												// for update & delete mode
			add(btnMemByEmail);
		add(infoEmail);
		if (currentMode == WINDOW_MODE.CREATE) // fill in empty column for
												// create mode
			add(new JLabel());

		add(lblAge);
		add(txtAge);
		add(new JLabel());
		add(new JLabel());

		add(lblGender);
		add(txtGender);
		add(infoGender);
		add(new JLabel());

		add(lblCity);
		add(txtCity);
		add(new JLabel());
		add(new JLabel());

		add(lblState);
		add(txtState);
		add(infoState);
		add(new JLabel());

		add(btnReset);
		add(btnUser);
		add(btnMainMenu);

		UserHandler handler = new UserHandler();
		btnReset.addActionListener(handler);
		btnUser.addActionListener(handler);
		btnMemByUsername.addActionListener(handler);
		btnMemByEmail.addActionListener(handler);
		btnMainMenu.addActionListener(handler);
	}

	private void BindMemberData(User tempUser) {
		txtUserName.setText(tempUser.username);
		txtEmail.setText(tempUser.email);
		txtAge.setText(String.valueOf(tempUser.age));
		txtGender.setText(tempUser.gender);
		txtCity.setText(tempUser.city);
		txtState.setText(tempUser.state);
	}

	/*
	 * Gets info from front-end and creates a new member
	 */
	private void CreateMember() {
		// boolean noInputError = true;
		DB_User db_user = new DB_User();

		if (IsInputError() == false) {
			user = GetUserInfo();
			if (db_user.CreateUser(user)) {
				ShowSuccessMsg();
			} else {
				ShowFailedMsg();
			}
		}
	}

	/*
	 * deletes selected user
	 */
	private void DeleteMember() {
		DB_User db_user = new DB_User();

		if (user != null) {
			if (db_user.DeleteUser(user)) {
				ShowSuccessMsg("delete success");
			} else {
				ShowFailedMsg("delete failed");
			}
		} else {
			ShowFailedMsg("no user selected");
		}
	}

	/**
	 * Get user information from front end
	 * 
	 * @return user information
	 */
	private User GetUserInfo() {

		// boolean noError = true;
		User newUser = new User();

		newUser.username = txtUserName.getText();
		newUser.email = txtEmail.getText();

		if (txtAge.getText().length() > 0) {
			// age
			newUser.age = Integer.parseInt(txtAge.getText());
		} else {
			// age field empty, put some value
			newUser.age = -1;
		}

		if (txtGender.getText().length() > 0) {
			newUser.gender = txtGender.getText();
		} else {
			newUser.gender = null;
		}

		if (txtCity.getText().length() > 0) {
			newUser.city = txtCity.getText();
		} else {
			newUser.city = null;
		}

		if (txtState.getText().length() == 2) {
			newUser.state = txtState.getText();
		} else {
			newUser.state = null;
		}

		// user = newUser;

		return newUser;
	} // end of InitializeUser

	/**
	 * Checks for input error
	 * 
	 * @return true if there is any error false otherwise
	 */
	private boolean IsInputError() {
		boolean isError = false;
		String strToCheck = null; // temporary string just to check the input
									// texts

		strToCheck = txtUserName.getText();
		if (strToCheck != null && strToCheck.length() != 0) {
			// username
			infoUserName.setText(lblInfoUnique);
			infoUserName.setForeground(Color.BLACK);
		} else {
			// username field empty
			isError = true;
			infoUserName.setText("can't be empty");
			infoUserName.setForeground(Color.RED);
		}

		if (txtEmail.getText().length() != 0 && txtEmail.getText() != null) {
			// email
			infoEmail.setText(lblInfoUnique);
			infoEmail.setForeground(Color.BLACK);
		} else {
			// email field empty
			isError = true;
			infoEmail.setText("can't be empty");
			infoEmail.setForeground(Color.RED);
		}

		// if(txtAge.getText().length() > 0)
		// {
		// // age
		// newUser.age = Integer.parseInt(txtAge.getText());
		// }
		// else
		// {
		// // age field empty, put some value
		// newUser.age = -1;
		// }

		strToCheck = txtGender.getText();
		if (strToCheck.equalsIgnoreCase("male")
				|| strToCheck.equalsIgnoreCase("female")
				|| strToCheck.length() == 0) {
			infoGender.setText(lblInfoGender);
			infoGender.setForeground(Color.BLACK);
		} else {
			isError = true;
			infoGender.setText("enter: male or female");
			infoGender.setForeground(Color.RED);
		}

		strToCheck = txtState.getText();
		if (strToCheck.length() == 2 || strToCheck.length() == 0) {
			// two letter state or empty
			infoState.setForeground(Color.BLACK);
		} else {
			isError = true;
			infoState.setForeground(Color.RED);
		}

		return isError;
	}

	private void ResetFields() {
		txtUserName.setText(null);
		txtEmail.setText(null);
		txtAge.setText(null);
		txtGender.setText(null);
		txtCity.setText(null);
		txtState.setText(null);
		user = null;
	}

	private void ShowMemberByEmail() {
		String lookupEmail = txtEmail.getText();
		DB_User db_user = new DB_User();

		if (lookupEmail.length() > 0) {
			user = db_user.GetUserByEmail(lookupEmail);
			if (user != null) {
				BindMemberData(user);
				infoEmail.setText(lblInfoUnique);
				infoEmail.setForeground(Color.BLACK);
			} else {
				infoEmail.setText("user does not exist");
				infoEmail.setForeground(Color.RED);
			}
		} else {
			infoEmail.setText("can't be empty");
			infoEmail.setForeground(Color.RED);
		}
	}

	/*
	 * Gets member info by using username
	 */
	private void ShowMemberByUsername() {
		String lookupUsername = txtUserName.getText();
		DB_User db_user = new DB_User();

		if (lookupUsername.length() > 0) {
			user = db_user.GetUserByUserName(lookupUsername);
			if (user != null) {
				BindMemberData(user);
				infoUserName.setText(lblInfoUnique);
				infoUserName.setForeground(Color.BLACK);
			} else {
				infoUserName.setText("user does not exist");
				infoUserName.setForeground(Color.RED);
			}
		} else {
			infoUserName.setText("can't be empty");
			infoUserName.setForeground(Color.RED);
		}
	}

	private void ShowSuccessMsg() {
		lblMessage.setText(lblInfoSuccess);
		lblMessage.setForeground(Color.GREEN);
	}

	private void ShowSuccessMsg(String msg) {
		lblMessage.setText(msg);
		lblMessage.setForeground(Color.GREEN);
	}

	private void ShowFailedMsg() {
		lblMessage.setText(lblInfoFailed);
		lblMessage.setForeground(Color.RED);
	}

	private void ShowFailedMsg(String msg) {
		lblMessage.setText(msg);
		lblMessage.setForeground(Color.RED);
	}

	/*
	 * gets value from front end and updates member
	 */
	private void UpdateMember() {
		DB_User db_user = new DB_User();

		if (user != null) {
			int memberId = user.id; // saving the id, bcz after initializing we
									// are gonna lose it
			if (IsInputError() == false) {
				user = GetUserInfo();

				user.id = memberId;

				if (db_user.UpdateUser(user)) {
					ShowSuccessMsg("update success");
				} else {
					ShowFailedMsg("update failed");
				}
			}
		} else {
			ShowFailedMsg("no user selected - search user by username/email");
		}
	}

	private class UserHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			// TODO Auto-generated method stub

			lblMessage.setText(""); // reset message

			if (event.getSource() == btnMainMenu) {
				UI_Main_Menu mainMenu = new UI_Main_Menu();
				mainMenu.setVisible(true);
				mainMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose();
			}
			if (event.getSource() == btnUser
					&& currentMode == WINDOW_MODE.CREATE) {
				CreateMember();
			} // end of if event = btnUser
			else if (event.getSource() == btnUser
					&& currentMode == WINDOW_MODE.UPDATE) {
				UpdateMember();
			} else if (event.getSource() == btnUser
					&& currentMode == WINDOW_MODE.DELETE) {
				DeleteMember();
			} else if (event.getSource() == btnReset) {
				ResetFields();
			} else if (event.getSource() == btnMemByUsername
					&& currentMode != WINDOW_MODE.CREATE) {
				ShowMemberByUsername();
			} else if (event.getSource() == btnMemByEmail
					&& currentMode != WINDOW_MODE.CREATE) {
				ShowMemberByEmail();
			}
		} // end of actionPerformed
	} // end of userhandler
}
