
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Color;
import java.awt.FlowLayout;

public class UI_Car_Review extends JFrame {

	private JList colorJList;
	// private final String colorNames[] = {"Black", "Blue", "Cyan",
	// "Dark Gray"};

	private final Color colors[] = { Color.black, Color.blue, Color.CYAN,
			Color.DARK_GRAY, Color.GREEN };

	// private ResultSetTableMode tableModel;

	public UI_Car_Review(String listNames[]) {
		super("List Test");
		setLayout(new FlowLayout()); // set frame layout

		colorJList = new JList(listNames);
		// colorJList = new JList();
		colorJList.setVisibleRowCount(5);

		colorJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		add(new JScrollPane(colorJList));

		colorJList.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent event) {
				// TODO Auto-generated method stub
				getContentPane().setBackground(
						colors[colorJList.getSelectedIndex()]);
			}
		});
	}

}
