import java.sql.Connection;
import java.sql.Statement;

public class DB_CreateDatabase {
	private String sqlMemberTable = "CREATE TABLE Member( "
			+ "Id        INTEGER NOT NULL PRIMARY KEY,"
			+ "Username  VARCHAR(30) NOT NULL UNIQUE,"
			+ "Email     VARCHAR(50) UNIQUE,"
			+ "Age       INTEGER, "
			+ "Gender    VARCHAR(6) CONSTRAINT Member_Chk_Gender CHECK (GENDER IN ('male', 'female')),"
			+ "City      VARCHAR(20)," + "State     CHAR(2) " + ")";
	private String sqlMemberSeq = "CREATE SEQUENCE Member_Id_Seq "
			+ "START WITH 1 " + "INCREMENT BY 1 ";
	private String sqlMemberTrg = "CREATE OR REPLACE TRIGGER MEMBER_PK_TRG "
			+ "BEFORE INSERT ON MEMBER " + "FOR EACH ROW " + "BEGIN "
			+ "IF INSERTING THEN "
			+ "SELECT MEMBER_ID_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL; "
			+ "END IF; " + "END;";

	private String sqlCarTable = "CREATE TABLE CAR( "
			+ " ID              INTEGER NOT NULL,"
			+ " MANUFACTURER    VARCHAR(50)," + " MAKE            VARCHAR(50),"
			+ " YEAR            INTEGER," + "  MODEL           VARCHAR(20)," +

			" CONSTRAINT CAR_PK PRIMARY KEY (ID)" + ")";
	private String sqlCarSeq = "CREATE SEQUENCE CAR_ID_SEQ "
			+ " START WITH 100" + " INCREMENT BY 1";
	private String sqlCarTrg = "CREATE OR REPLACE TRIGGER CAR_ID_TRG "
			+ "BEFORE INSERT ON CAR " + "FOR EACH ROW " + "BEGIN "
			+ "IF INSERTING THEN "
			+ "SELECT CAR_ID_SEQ.nextval INTO :NEW.ID FROM SYS.DUAL; "
			+ "END IF; " + "END;";

	private String sqlFeatureTable = "CREATE TABLE Feature( "
			+ "Id          INTEGER CONSTRAINT Feature_PK NOT NULL PRIMARY KEY, "
			+ "Name        VARCHAR(30), " + "Description VARCHAR(1000) " + ")";
	private String sqlFeatureSeq = "CREATE SEQUENCE FEATURE_ID_SEQ "
			+ "START WITH 1000 " + "INCREMENT BY 1";
	private String sqlFeatureTrg = "CREATE OR REPLACE TRIGGER FEATURE_TRG "
			+ "BEFORE INSERT ON FEATURE " + "FOR EACH ROW " + "BEGIN "
			+ "IF INSERTING THEN "
			+ "SELECT FEATURE_ID_SEQ.nextval INTO :NEW.ID FROM SYS.DUAL; "
			+ "END IF; " + "END;";

	private String sqlReviewTable = "CREATE TABLE Review( "
			+ "Id    INTEGER CONSTRAINT Review_PK NOT NULL PRIMARY KEY, "
			+ "Description VARCHAR(2000) " + ")";
	private String sqlReviewSeq = "CREATE SEQUENCE Review_Id_Seq "
			+ "START WITH 1 " + "INCREMENT BY 1";
	private String sqlReviewTrg = "CREATE OR REPLACE TRIGGER Review_Trg "
			+ "BEFORE INSERT ON Review " + "FOR EACH ROW " + "BEGIN "
			+ "IF INSERTING THEN "
			+ "SELECT Review_Id_Seq.nextval INTO :NEW.Id FROM SYS.DUAL; "
			+ "END IF; " + "END;";

	private String sqlLkRatingTable = "CREATE TABLE LOOKUP_RATING( "
			+ "RATING                INTEGER NOT NULL, "
			+ "RATING_DESCRIPTION    VARCHAR(30) NOT NULL, " +

			"CONSTRAINT LOOKUP_RATING_PK " + "PRIMARY KEY(RATING) " + ")";

	private String sqlCarReviewTable = "CREATE TABLE CAR_FEATURE( "
			+ "CAR_ID      INTEGER NOT NULL, "
			+ "FEATURE_ID  INTEGER NOT NULL, " +

			"CONSTRAINT CAR_FEATURE_PK " + "PRIMARY KEY (CAR_ID, FEATURE_ID), "
			+ "CONSTRAINT CAR_FEATURE_CAR_ID_FK "
			+ "FOREIGN KEY (CAR_ID) REFERENCES CAR(ID), "
			+ "CONSTRAINT CAR_FEATURE_FEATURE_ID_FK "
			+ "FOREIGN KEY (FEATURE_ID) REFERENCES FEATURE(ID) " + ")";

	private String sqlMemCarRevTable = "CREATE TABLE MEMBER_CAR_REVIEW( "
			+ "MEM_ID      INTEGER NOT NULL, "
			+ "CAR_ID      INTEGER NOT NULL, "
			+ "REV_ID      INTEGER NOT NULL, " +

			"CONSTRAINT MEM_CAR_REV_PK " + "PRIMARY KEY (REV_ID), " +

			"CONSTRAINT MEM_CAR_REV_UNIQUE " + "UNIQUE (MEM_ID, CAR_ID), "
			+ "CONSTRAINT MEM_CAR_REV_MEM_USRNAME_FK "
			+ "FOREIGN KEY (MEM_ID) REFERENCES MEMBER(ID), "
			+ "CONSTRAINT MEM_CAR_REV_REV_ID_FK "
			+ "FOREIGN KEY (REV_ID) REFERENCES REVIEW(ID), "
			+ "CONSTRAINT MEM_CAR_REV_CAR_ID_FK "
			+ "FOREIGN KEY (CAR_ID) REFERENCES CAR(ID) " + ")";

	private String sqlRevListFeatTable = "CREATE TABLE REVIEW_LIST_FEATURE( "
			+ "REV_ID        INTEGER NOT NULL, "
			+ "FEATURE_ID    INTEGER NOT NULL, " + "FEATURE_ORDER INTEGER, " +

			"CONSTRAINT REV_LIST_FEAT_PK "
			+ "PRIMARY KEY (REV_ID, FEATURE_ID), "
			+ "CONSTRAINT REV_LIST_FEAT_REV_ID_FK "
			+ "FOREIGN KEY (REV_ID) REFERENCES REVIEW(ID), "
			+ "CONSTRAINT REV_LIST_FEAT_FEATURE_ID_FK "
			+ "FOREIGN KEY (FEATURE_ID) REFERENCES FEATURE(ID) " + ")";

	private String sqlMemRateRev = "CREATE TABLE Member_Rate_Review( "
			+ "Mem_ID      INTEGER NOT NULL, "
			+ "Rev_Id      INTEGER NOT NULL, "
			+ "Rating      INTEGER NOT NULL, " + "CONSTRAINT MemRateRev_PK "
			+ "PRIMARY KEY (Mem_ID, Rev_Id), "
			+ "CONSTRAINT MemRateRev_MemUsrName_FK "
			+ "FOREIGN KEY (Mem_ID) REFERENCES Member(ID), "
			+ "CONSTRAINT MemRateRev_RevId_FK "
			+ "FOREIGN KEY (Rev_Id) REFERENCES Review(Id), "
			+ "CONSTRAINT MemRateRev_Rating_FK "
			+ "FOREIGN KEY (Rating) REFERENCES LookUp_Rating(Rating) " + ")";

	private String sqlProcInsertMem = "CREATE OR REPLACE PROCEDURE "
			+ "INSERT_MEMBER " + "( " + "pUsername  VARCHAR, "
			+ "pEmail     VARCHAR, " + "pAge       INTEGER, "
			+ "pGender    VARCHAR, " + "pCity      VARCHAR, "
			+ "pState     CHAR " + ") " + "AS " +

			"BEGIN " + "INSERT INTO MEMBER " + "( " + "Username, " + "Email, "
			+ "Age, " + "Gender, " + "City, " + "State " + ") " + "VALUES "
			+ "( " + "pUsername, " + "pEmail, " + "pAge, " + "Lower(pGender), "
			+ "pCity, " + "pState " + "); " + "END;";

	private String sqlProcDeleteMemberByID = "CREATE OR REPLACE PROCEDURE "
			+ "DELETE_MEMBER_BY_ID " + "( " + "p_Id   INTEGER " + ") " + "AS " +

			"BEGIN " + "DELETE FROM member_car_review "
			+ "WHERE mem_id = p_Id; " + "DELETE FROM member_rate_review "
			+ "WHERE mem_id = p_Id; " + "DELETE FROM member "
			+ "WHERE id = p_Id; " + "END;";

	private String sqlProcSelAllMem = "CREATE OR REPLACE PROCEDURE "
			+ "SELECT_ALL_MEMBER " + "( "
			+ "pMember_Recordset  OUT   SYS_REFCURSOR " + ") " + "AS " +

			"BEGIN " + "OPEN pMember_Recordset FOR " + "SELECT " + "Id, "
			+ "Username, " + "Email, " + "Age, " + "Gender, " + "City, "
			+ "State " + "FROM    member; " + "END;";

	private String sqlProcSelMemEmail = "create or replace PROCEDURE "
			+ "SELECT_MEMBER_BY_EMAIL " + "( " + "p_Email    IN VARCHAR, "
			+ "p_Id       OUT INTEGER, " + "p_Username OUT  VARCHAR, "
			+ "p_Age      OUT INTEGER, " + "p_Gender   OUT VARCHAR, "
			+ "p_City     OUT VARCHAR, " + "p_State    OUT VARCHAR " + ") "
			+ "AS " +

			"BEGIN " + "SELECT " + "Id, " + "Username, " + "Age, " + "Gender, "
			+ "City, " + "State " + "INTO " + "p_Id, " + "p_Username, "
			+ "p_Age, " + "p_Gender, " + "p_City, " + "p_State " +

			"FROM    member " + "WHERE   Email = p_Email; " + "END;";

	private String sqlProcSelMemByUsrname = "create or replace PROCEDURE "
			+ "SELECT_MEMBER_BY_USERNAME " + "( " + "p_Username IN  VARCHAR, "
			+ "p_Id       OUT INTEGER, " + "p_Email    OUT VARCHAR, "
			+ "p_Age      OUT INTEGER, " + "p_Gender   OUT VARCHAR, "
			+ "p_City     OUT VARCHAR, " + "p_State    OUT VARCHAR " + ") "
			+ "AS " +

			"BEGIN " + "SELECT " + "Id, " + "Email, " + "Age, " + "Gender, "
			+ "City, " + "State " + "INTO " + "p_Id, " + "p_Email, "
			+ "p_Age, " + "p_Gender, " + "p_City, " + "p_State " +

			"FROM    member " + "WHERE   Username = p_username; " + "END;";

	private String sqlProcUpdMemById = "create or replace PROCEDURE "
			+ "UPDATE_MEMBER_BY_ID " + "( " + "pId        INTEGER, "
			+ "pUsername  VARCHAR, " + "pEmail     VARCHAR, "
			+ "pAge       INTEGER, " + "pGender    VARCHAR, "
			+ "pCity      VARCHAR, " + "pState     CHAR " + ") " + "AS " +

			"BEGIN " + "UPDATE member " + "SET " + "username  = pUsername, "
			+ "email     = pEmail, " + "age       = pAge, "
			+ "gender    = pGender, " + "city      = pCity, "
			+ "state     = pState " + "WHERE Id = pId; " +

			"COMMIT WORK; " + "END;";

	private String sqlMockInsertCar[] = {
			"INSERT INTO Car ( Manufacturer, Make, Model, Year) "
					+ "VALUES ( 'Toyota', 'Toyota', 'Yaris', 2013) ",

			"INSERT INTO Car (Manufacturer, Make, Model, Year) "
					+ "VALUES ('Toyota', 'Toyota', 'Yaris', 2012) ",

			"INSERT INTO Car (Manufacturer, Make, Model, Year) "
					+ "VALUES ('Honda', 'Honda', 'Acura', 2012) ",

			"INSERT INTO Car (Manufacturer, Make, Model, Year) "
					+ "VALUES ('Chevrolet', 'Chevrolet', 'Travarse', 2013) ",

			"INSERT INTO Car (Manufacturer, Make, Model, Year) "
					+ "VALUES ('GM', 'GM', 'SomeModel', 2013) ",

			"INSERT INTO Car (Manufacturer, Make, Model, Year) "
					+ "VALUES ('BMW', 'BMW', 'TestModel', 2010) ",

			"INSERT INTO Car (Manufacturer, Make, Model, Year) "
					+ "VALUES ('Honda', 'Honda', 'Acura', 2010) ",

			"INSERT INTO Car (Manufacturer, Make, Model, Year) "
					+ "VALUES ('Honda', 'Honda', 'LX', 2012) ",

			"INSERT INTO Car (Manufacturer, Make, Model, Year) "
					+ "VALUES ('Nissan', 'Nissan', 'Roster', 1995) ",

			"INSERT INTO Car (Manufacturer, Make, Model, Year) "
					+ "VALUES ('Toyota', 'Toyouta', 'Corolla', 2000)" };

	private String sqlMockInsertFeature[] = {
			"INSERT INTO Feature(Name, Description) "
					+ "VALUES ('Leather Seat', 'All the seats have leather seat, and it is awesome')",

			"INSERT INTO Feature(Name, Description) "
					+ "VALUES ('Moonroof', '4 feet long moon roof, and it is awesome')",

			"INSERT INTO Feature(Name, Description) "
					+ "VALUES ('All wheel drive', 'All wheel drive is awesome')",

			"INSERT INTO Feature(Name, Description) "
					+ "VALUES ('Two door', 'Two door is awesome')",

			"INSERT INTO Feature(Name, Description) "
					+ "VALUES ('Some Feature', 'I am not very good with feature, how many features can a car have?')",

			"INSERT INTO Feature( Name, Description) "
					+ "VALUES ( 'Some more features', 'Ok I need ten features')",

			"INSERT INTO Feature( Name, Description) "
					+ "VALUES ( 'Another Feature', 'Seven done, need three more, I can add those tmw')",

			"INSERT INTO Feature( Name, Description) "
					+ "VALUES ('Cool Feature', 'Eight done, need two more, I can add those tmw')",

			"INSERT INTO Feature( Name, Description) "
					+ "VALUES ( 'Another Cool Feature', '9 done, need one more, I can add those tmw')",

			"INSERT INTO Feature( Name, Description) "
					+ "VALUES ( 'Super Cool Feature', '10 done, Im done')" };

	private String sqlMockInsertRating[] = {
			"INSERT INTO LOOKUP_RATING (RATING, RATING_DESCRIPTION) VALUES (1, 'Misleading')",
			"INSERT INTO LOOKUP_RATING (RATING, RATING_DESCRIPTION) VALUES (2, 'Not Helpful')",
			"INSERT INTO LOOKUP_RATING (RATING, RATING_DESCRIPTION) VALUES (3, 'Helpful')",
			"INSERT INTO LOOKUP_RATING (RATING, RATING_DESCRIPTION) VALUES (4, 'Very Helpful')",
			"INSERT INTO LOOKUP_RATING (RATING, RATING_DESCRIPTION) VALUES (5, 'Extremely Helpful')" };

	private String sqlMockInsertMember[] = {
			"INSERT INTO Member(Username, Email, Age, Gender, City, State) "
					+ "VALUES ('abcdefg', 'abcdefg@yahoo.com', 32, LOWER('Male'), 'Detroit', 'MI')",

			"INSERT INTO Member(Username, Email, Age, Gender, City, State) "
					+ "VALUES ('qwert', 'qwert@yahoo.com', 18, LOWER('FeMale'), 'Dearborn', 'MI')",

			"INSERT INTO Member(Username, Email, Age, Gender, City, State) "
					+ "VALUES ('blackcloud', 'blackcloud@yahoo.com', 27, 'male', 'Los Angeles', 'CA')",

			"INSERT INTO Member(Username, Email, Age, Gender, City, State) "
					+ "VALUES ('gajakhuri', 'gjkhuri@gmail.com', 32, 'female', 'Detroit', 'MI')",

			"INSERT INTO Member(Username, Email, Age, Gender, City, State) "
					+ "VALUES ('teemo', 'teemo@yahoo.com', 13, LOWER('Male'), 'Detroit', 'MI')",

			"INSERT INTO Member(Username, Email, Age, Gender, City, State) "
					+ "VALUES ('Garen', 'qres@yahoo.com', 18, LOWER('FeMale'), 'Dearborn', 'MI')",

			"INSERT INTO Member(Username, Email, Age, Gender, City, State) "
					+ "VALUES ('nidaee', 'nid@yahoo.com', 27, 'female', 'Los Angeles', 'CA')",

			"INSERT INTO Member(Username, Email, Age, Gender, City, State) "
					+ "VALUES ('shau', 'shayn@gmail.com', 5, 'male', 'Detroit', 'MI')",

			"INSERT INTO Member(Username, Email, Age, Gender, City, State) "
					+ "VALUES ('roku', 'abcdefg@rsdf.com', 32, LOWER('Male'), 'Detroit', 'MI')",

			"INSERT INTO Member(Username, Email, Age, Gender, City, State) "
					+ "VALUES ('Yi', 'yi@yhotma.com', 18, LOWER('Male'), 'Dearborn', 'MI')" };

	private String sqlMockInsertReview[] = {
			"INSERT INTO Review( Description) "
					+ "VALUES ('I like this this and this feature of this car because it is awesome')",

			"INSERT INTO Review( Description) "
					+ "VALUES ('I like Honda because it is awesome')",

			"INSERT INTO Review( Description) "
					+ "VALUES ('I like this car because it is awesome')",

			"INSERT INTO Review( Description) "
					+ "VALUES ('I like to use the word awesome bcz it is awesome')",

			"INSERT INTO Review( Description) "
					+ "VALUES ('Gotta think about single quot') ",

			"INSERT INTO Review( Description) "
					+ "VALUES ('This car has better fuel efficiency than the other cars and it is awesome')",

			"INSERT INTO Review( Description) "
					+ "VALUES ('I like this car because it is awesome')",

			"INSERT INTO Review( Description) "
					+ "VALUES ('I like to use the word awesome bcz it is awesome')",

			"INSERT INTO Review( Description) "
					+ "VALUES ('Gotta think about single quot')",

			"INSERT INTO Review( Description) "
					+ "VALUES ('This car has better fuel efficiency than the other cars and it is awesome')" };

	private String sqlMockInsertCarFeat[] = {
			"INSERT INTO CAR_FEATURE (car_id, feature_id)"
					+ "VALUES (100, 1008)",
			"INSERT INTO CAR_FEATURE (car_id, feature_id)"
					+ "VALUES (100, 1005)",
			"INSERT INTO CAR_FEATURE (car_id, feature_id)"
					+ "VALUES (101, 1001)",
			"INSERT INTO CAR_FEATURE (car_id, feature_id)"
					+ "VALUES (101, 1002)",
			"INSERT INTO CAR_FEATURE (car_id, feature_id)"
					+ "VALUES (102, 1003)",
			"INSERT INTO CAR_FEATURE (car_id, feature_id)"
					+ "VALUES (102, 1005)",
			"INSERT INTO CAR_FEATURE (car_id, feature_id)"
					+ "VALUES (102, 1006)",
			"INSERT INTO CAR_FEATURE (car_id, feature_id)"
					+ "VALUES (105, 1002)",
			"INSERT INTO CAR_FEATURE (car_id, feature_id)"
					+ "VALUES (105, 1003)",
			"INSERT INTO CAR_FEATURE (car_id, feature_id)"
					+ "VALUES (107, 1008)" };

	private String sqlMockInsertMemCarRev[] = {
			"INSERT INTO MEMBER_CAR_REVIEW (car_id, mem_id, rev_id)"
					+ "VALUES (100, 1, 1)",
			"INSERT INTO MEMBER_CAR_REVIEW (car_id, mem_id, rev_id)"
					+ "VALUES (101, 1, 2)",
			"INSERT INTO MEMBER_CAR_REVIEW (car_id, mem_id, rev_id)"
					+ "VALUES (100, 2, 3)",
			"INSERT INTO MEMBER_CAR_REVIEW (car_id, mem_id, rev_id)"
					+ "VALUES (102, 3, 4)",
			"INSERT INTO MEMBER_CAR_REVIEW (car_id, mem_id, rev_id)"
					+ "VALUES (104, 4, 5)",
			"INSERT INTO MEMBER_CAR_REVIEW (car_id, mem_id, rev_id)"
					+ "VALUES (102, 5, 6)",
			"INSERT INTO MEMBER_CAR_REVIEW (car_id, mem_id, rev_id)"
					+ "VALUES (109, 5, 7)",
			"INSERT INTO MEMBER_CAR_REVIEW (car_id, mem_id, rev_id)"
					+ "VALUES (107, 9, 8)",
			"INSERT INTO MEMBER_CAR_REVIEW (car_id, mem_id, rev_id)"
					+ "VALUES (105, 6, 9)",
			"INSERT INTO MEMBER_CAR_REVIEW (car_id, mem_id, rev_id)"
					+ "VALUES (103, 6, 10)" };

	private String sqlMockInsertMemRateRev[] = {
			"INSERT INTO MEMBER_RATE_REVIEW (mem_id, rev_id, rating)"
					+ "VALUES(1, 2, 3)",
			"INSERT INTO MEMBER_RATE_REVIEW (mem_id, rev_id, rating)"
					+ "VALUES(1, 3, 4)",
			"INSERT INTO MEMBER_RATE_REVIEW (mem_id, rev_id, rating)"
					+ "VALUES(2, 1, 5)",
			"INSERT INTO MEMBER_RATE_REVIEW (mem_id, rev_id, rating)"
					+ "VALUES(3, 1, 1)",
			"INSERT INTO MEMBER_RATE_REVIEW (mem_id, rev_id, rating)"
					+ "VALUES(4, 1, 3)",
			"INSERT INTO MEMBER_RATE_REVIEW (mem_id, rev_id, rating)"
					+ "VALUES(5, 3, 3)",
			"INSERT INTO MEMBER_RATE_REVIEW (mem_id, rev_id, rating)"
					+ "VALUES(5, 6, 4)",
			"INSERT INTO MEMBER_RATE_REVIEW (mem_id, rev_id, rating)"
					+ "VALUES(6, 8, 3)",
			"INSERT INTO MEMBER_RATE_REVIEW (mem_id, rev_id, rating)"
					+ "VALUES(7, 2, 3)",
			"INSERT INTO MEMBER_RATE_REVIEW (mem_id, rev_id, rating)"
					+ "VALUES(4, 8, 4)" };

	private String sqlMockInsertRevListFeat[] = {
			"INSERT INTO REVIEW_LIST_FEATURE (rev_id, feature_id, feature_order)"
					+ "VALUES (1, 1008, 1)",
			"INSERT INTO REVIEW_LIST_FEATURE (rev_id, feature_id, feature_order)"
					+ "VALUES (1, 1005, 2)",
			"INSERT INTO REVIEW_LIST_FEATURE (rev_id, feature_id, feature_order)"
					+ "VALUES (2, 1001, 1)",
			"INSERT INTO REVIEW_LIST_FEATURE (rev_id, feature_id, feature_order)"
					+ "VALUES (2, 1002, 2)",
			"INSERT INTO REVIEW_LIST_FEATURE (rev_id, feature_id, feature_order)"
					+ "VALUES (3, 1005, 1)",
			"INSERT INTO REVIEW_LIST_FEATURE (rev_id, feature_id, feature_order)"
					+ "VALUES (3, 1008, 2)",
			"INSERT INTO REVIEW_LIST_FEATURE (rev_id, feature_id, feature_order)"
					+ "VALUES (5, 1005, 1)",
			"INSERT INTO REVIEW_LIST_FEATURE (rev_id, feature_id, feature_order)"
					+ "VALUES (4, 1006, 1)",
			"INSERT INTO REVIEW_LIST_FEATURE (rev_id, feature_id, feature_order)"
					+ "VALUES (9, 1003, 1)",
			"INSERT INTO REVIEW_LIST_FEATURE (rev_id, feature_id, feature_order)"
					+ "VALUES (8, 1007, 1)" };

	/**
	 * Creates table in DB
	 * 
	 * @return true on success
	 */
	private boolean CreateTables() {
		Connection conn = null;
		Statement stmt = null;
		DatabaseConnection dc = new DatabaseConnection();
		DatabaseConnector db = dc.getConnector();
		conn = db.getConnection();
		if (conn != null)
			System.out.println("Connection Made Table");
		try {
			stmt = conn.createStatement();
			stmt.execute(sqlMemberTable);
			stmt.execute(sqlMemberSeq);
			stmt.execute(sqlMemberTrg);

			stmt.execute(sqlCarTable);
			stmt.execute(sqlCarSeq);
			stmt.execute(sqlCarTrg);

			stmt.execute(sqlFeatureTable);
			stmt.execute(sqlFeatureSeq);
			stmt.execute(sqlFeatureTrg);

			stmt.execute(sqlReviewTable);
			stmt.execute(sqlReviewSeq);
			stmt.execute(sqlReviewTrg);

			stmt.execute(sqlLkRatingTable);

			stmt.execute(sqlCarReviewTable);

			stmt.execute(sqlMemCarRevTable);

			stmt.execute(sqlRevListFeatTable);

			stmt.execute(sqlMemRateRev);

			return true;
		} catch (Exception ex) {
			System.out.println("Error: " + ex);
			return false;
		}

	}

	/**
	 * Creates procedures
	 * 
	 * @return true on success
	 */
	private boolean CreateProcedures() {
		Connection conn = null;
		Statement stmt = null;
		DatabaseConnection dc = new DatabaseConnection();
		DatabaseConnector db = dc.getConnector();
		conn = db.getConnection();
		
		if (conn != null)
			System.out.println("Connection Made Procedure");

		try {
			stmt = conn.createStatement();
			stmt.execute(sqlProcInsertMem);

			stmt.execute(sqlProcDeleteMemberByID);

			stmt.execute(sqlProcSelAllMem);

			stmt.execute(sqlProcSelMemEmail);

			stmt.execute(sqlProcSelMemByUsrname);

			stmt.execute(sqlProcUpdMemById);

			return true;
		} catch (Exception ex) {
			System.out.println("Error: " + ex);
			return false;
		}
	}

	/**
	 * Inserts data into tables
	 * 
	 * @return true on success
	 */
	private boolean InsertMockRecord() {
		Connection conn = null;
		Statement stmt = null;
		DatabaseConnection dc = new DatabaseConnection();
		DatabaseConnector db = dc.getConnector();
		conn = db.getConnection();
		
		if (conn != null)
			System.out.println("Connection Made Mock Record");
		try {
			stmt = conn.createStatement();
			for (int i = 0; i < sqlMockInsertCar.length; i++) {
				stmt.addBatch(sqlMockInsertCar[i]);
			}
			for (int i = 0; i < sqlMockInsertFeature.length; i++) {
				stmt.addBatch(sqlMockInsertFeature[i]);
			}
			for (int i = 0; i < sqlMockInsertRating.length; i++) {
				stmt.addBatch(sqlMockInsertRating[i]);
			}
			for (int i = 0; i < sqlMockInsertMember.length; i++) {
				stmt.addBatch(sqlMockInsertMember[i]);
			}
			for (int i = 0; i < sqlMockInsertReview.length; i++) {
				stmt.addBatch(sqlMockInsertReview[i]);
			}
			for (int i = 0; i < sqlMockInsertCarFeat.length; i++) {
				stmt.addBatch(sqlMockInsertCarFeat[i]);
			}
			for (int i = 0; i < sqlMockInsertMemCarRev.length; i++) {
				stmt.addBatch(sqlMockInsertMemCarRev[i]);
			}
			for (int i = 0; i < sqlMockInsertMemRateRev.length; i++) {
				stmt.addBatch(sqlMockInsertMemRateRev[i]);
			}
			for (int i = 0; i < sqlMockInsertRevListFeat.length; i++) {
				stmt.addBatch(sqlMockInsertRevListFeat[i]);
			}
			stmt.executeBatch();
			stmt.clearBatch();

			return true;
		} catch (Exception ex) {
			System.out.println("Error Mock Record: " + ex);
			return false;
		}
	}

	/**
	 * Creates table, procedure Insert mock records
	 * 
	 * @return true on success
	 */
	public boolean CreateDB() {
		boolean noError = false;
		noError = CreateTables();
		noError = CreateProcedures();
		noError = InsertMockRecord();

		return noError; // no failure - return success
						// or return false bcz one of them failed
	}

	// public static void main(String[] args)
	// {
	// DB_CreateDatabase createDB = new DB_CreateDatabase();
	// createDB.CreateDB();
	// }
}
