import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;


public class DB_Review {

	private DatabaseConnection dbConnection;
	private DatabaseConnector dbConnector;

	public DB_Review() {
		// TODO Auto-generated constructor stub
		dbConnection = new DatabaseConnection();
		dbConnector = dbConnection.getConnector();
	}
	
	public boolean CreateReview(Review review)
	{
		Connection conn;
		CallableStatement cs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call INSERT_REVIEW(?, ?, ?, ?)}");
			cs.setInt("pMem_Id", review.memberId);
			cs.setInt("pCar_Id", review.carId);
			cs.setString("pDescription", review.description);
			cs.registerOutParameter("nReview_Id", OracleTypes.INTEGER);
			cs.execute();
			
			review.id = cs.getInt("nReview_Id");
			
			if(review.featureIdList != null && review.featureIdList.size() > 0)
			{
				AddFeature(review.id, review.featureIdList);
			}
			return true;
		}
		catch(Exception ex){
			System.out.println("CreateReview Error: " + ex);
			return false;
		}
	}

	private boolean DeleteFeatureList(int revId)
	{
		Connection conn;
		CallableStatement cs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call delete_review_list_feature(?)}");
			cs.setInt("p_rev_id", revId);
			cs.execute();
			return true;
		}
		catch(Exception ex)
		{
			System.out.println("DeleteFeatureList Error: " + ex);
			return false;
		}
	}
	
	public void AddFeature(int revId, List<Integer> featureList)
	{
		Connection conn;
		CallableStatement cs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call INSERT_REVIEW_LIST_FEATURE(?, ?, ?)}");
			
			for(int index = 0; index < featureList.size(); index++){
				cs.setInt("p_rev_id", revId);
				cs.setInt("p_feature_id", featureList.get(index));
				cs.setInt("p_feature_order", index + 1);
				cs.execute();
			}
		}
		catch(Exception ex)
		{
			System.out.println("AddFeature Error: " + ex);
			return ;
		}
	}
	
	public boolean CreateRating(Review review)
	{
		Connection conn;
		CallableStatement cs;
		
		try
		{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call insert_review_rating(?, ?, ?)}");
			cs.setInt("p_mem_id", review.memberId);
			cs.setInt("p_rev_id", review.id);
			cs.setInt("p_rating", review.rating);
			cs.execute();
			return true;
		}
		catch(Exception ex)
		{
			System.out.println("CreateRating Error: " + ex);
			return false;
		}
	}
	
	public boolean UpdateRating(Review review)
	{
		Connection conn;
		CallableStatement cs;
		
		try
		{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call update_review_rating(?, ?, ?)}");
			cs.setInt("p_member_id", review.memberId);
			cs.setInt("p_review_id", review.id);
			cs.setInt("p_rating", review.rating);
			cs.execute();
			return true;
		}
		catch(Exception ex)
		{
			System.out.println("UpdateRating Error: " + ex);
			return false;
		}
	}
	
	public boolean DeleteRating(int memId, int revId)
	{
		Connection conn;
		CallableStatement cs;
		
		try
		{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call delete_review_rating(?, ?)}");
			cs.setInt("p_member_id", memId);
			cs.setInt("p_review_id", revId);
			cs.execute();
			return true;
		}
		catch(Exception ex)
		{
			System.out.println("DeleteRating Error: " + ex);
			return false;
		}
	}
	
	public boolean DeleteReview(int reviewId)
	{
		Connection conn;
		CallableStatement cs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call delete_review_by_id(?)}");
			cs.setInt("p_id", reviewId);
			cs.execute();
			return true;
		}
		catch(Exception ex)
		{
			System.out.println("DeleteReview Error" + ex);
			return false;
		}
	}
	
	private ResultSet GetAllRating()
	{
		Connection conn;
		CallableStatement cs;
		ResultSet rs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("begin select_all_rating(?); end;");
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = ((OracleCallableStatement) cs).getCursor(1);
			
			return rs;
		}
		catch(Exception ex)
		{
			System.out.println("GetAllReview Error: " + ex);
			return null;
		}
	}
	
	public List<String> GetAllRatingId()
	{
		try{
			ResultSet rs = GetAllRating();
			
			List<String> listRatingId = new ArrayList<String>();
			
			while(rs.next()){
				int ratingId = rs.getInt("rating");
				listRatingId.add(String.valueOf(ratingId));
			}

			return listRatingId;
		}
		catch(Exception ex)
		{
			System.out.println("GetAllRatingId error: " + ex);
			return null;
		}
	}
	
	private ResultSet GetAllReview()
	{
		Connection conn;
		CallableStatement cs;
		ResultSet rs;

		try {
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("begin select_all_review(?); end;");
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = ((OracleCallableStatement) cs).getCursor(1);
			return rs;
		} catch (Exception ex) {
			System.out.println("GetAllReview Error: " + ex);
			return null;
		}
	}
	
	public List<String> GetAllReviewId()
	{
		try{
			ResultSet rs = GetAllReview();
			
			List<String> listReviewId = new ArrayList<String>();
			
			while(rs.next()){
				int reviewId = rs.getInt("Id");
				listReviewId.add(String.valueOf(reviewId));
			}

			return listReviewId;
		}
		catch(Exception ex)
		{
			System.out.println("GetAllReviewId error: " + ex);
			return null;
		}
	}

	public Review GetReviewById(int reviewId)
	{
		Connection conn;
		CallableStatement cs;
		Review review = new Review();

		try {
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call select_review_by_id(?, ?, ?, ?)}");
			cs.setInt("p_review_id", reviewId);
			cs.registerOutParameter("o_mem_id", OracleTypes.INTEGER);
			cs.registerOutParameter("o_car_id", OracleTypes.INTEGER);
			cs.registerOutParameter("o_description", OracleTypes.VARCHAR);

			cs.execute();

			review.id = reviewId;
			review.memberId = cs.getInt("o_mem_id");
			review.carId = cs.getInt("o_car_id");
			review.description = cs.getString("o_description");

			return review;
		}

		catch (Exception ex) 
		{
			System.out.println("GetReviewById Error: " + ex);
			return null;
		}
	}

	public boolean UpdateReviewById(Review review)
	{
		Connection conn;
		CallableStatement cs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call update_review_by_id(?, ?)}");
			cs.setInt("p_Id", review.id);
			cs.setString("p_Description", review.description);
			cs.execute();
			
			if(review.featureIdList != null && review.featureIdList.size() > 0)
			{
				// delete all feature if exist (for update)
				DeleteFeatureList(review.id);
				AddFeature(review.id, review.featureIdList);
			}

			return true;
		}
		catch(Exception ex)
		{
			System.out.println("UpdateReviewById Error: " + ex);
			return false;
		}
	}

}
