import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class UI_Review extends JFrame{
	
	// shows information about insert/update/delete operation
	// e.g. update successful/couldn't create user
	private JLabel lblMessage;
	private String lblInfoSuccess;
	private String lblInfoFailed;
	
	private JComboBox<String> ddReviewId;
	
	private JComboBox<String> ddMemberId;
	private JTextField txtMemberId;		// to show member id during update/delete (read only)
	
//	private JLabel lblCarId;
	private JComboBox<String> ddCarId;
	private JTextField txtCarId;		// to show car id during update/delete (read only)
		
//	private JLabel lblDescription;
	private JTextField txtDescription;
	
	private JComboBox<String> ddFeatureId;
	private DefaultComboBoxModel<String> modelFeatureList;
	
	private JButton btnFeature;
	
	private JButton btnReset;
	private JButton btnSubmit;
	private JButton btnMainMenu;
	
	public static enum REVIEW_WINDOW_MODE
	{
		CREATE,
		UPDATE,
		DELETE
	}
	
	private REVIEW_WINDOW_MODE currentWindowMode;
	
	List<Integer> featureList;
	
	public UI_Review(REVIEW_WINDOW_MODE windowMode)
	{
		setTitle("Review");
		currentWindowMode = windowMode;
		CreateWindow();
		featureList = new ArrayList<Integer>();
	}
	
	private void CreateWindow()
	{
		setLayout(new GridLayout(6, 4));
		setLocation(500, 300);
		setSize(500, 300);

		lblMessage = new JLabel();
		
		if(currentWindowMode != REVIEW_WINDOW_MODE.CREATE)
		{
			ddReviewId = new JComboBox<String>(GetAllReviewId());
			
			txtMemberId = new JTextField(20);
			txtMemberId.setEditable(false);
			
			txtCarId = new JTextField();
			txtCarId.setEditable(false);
		}
		
		if(currentWindowMode == REVIEW_WINDOW_MODE.CREATE)
		{
			ddMemberId = new JComboBox<String>(GetAllMemberId());
			
			ddCarId = new JComboBox<String>(GetAllCarId());
		}
		
		txtDescription = new JTextField(20);
		
		modelFeatureList = new DefaultComboBoxModel<String>();
		ddFeatureId = new JComboBox<String>(modelFeatureList);
		
		btnFeature = new JButton("Add Feature");
		
		btnReset = new JButton("Reset");
		String btnName = "Create Review"; //default
		if(currentWindowMode == REVIEW_WINDOW_MODE.UPDATE)
		{
			btnName = "Update Review";
		}
		else if(currentWindowMode == REVIEW_WINDOW_MODE.DELETE)
		{
			btnName = "Delete Review";
		}
		btnSubmit = new JButton(btnName);
		btnMainMenu = new JButton("Main Menu");
		
		add(lblMessage);
		if(currentWindowMode != REVIEW_WINDOW_MODE.CREATE)
		{
			add(ddReviewId);
		}
		else
		{
			add(new JLabel());
		}
		add(new JLabel("instruction"));
		add(new JLabel());

		add(new JLabel("Reviwer Id: "));
		if(currentWindowMode == REVIEW_WINDOW_MODE.CREATE)
		{
			add(ddMemberId);
		}
		else
		{
			add(txtMemberId);
		}
		add(new JLabel());
		add(new JLabel());
		
		add(new JLabel("Car Id: "));
		if(currentWindowMode == REVIEW_WINDOW_MODE.CREATE)
		{
			add(ddCarId);
		}
		else
		{
			add(txtCarId);
		}
		add(new JLabel());
		add(new JLabel());
		
		add(new JLabel("Description"));
		add(txtDescription);
		add(new JLabel());
		add(new JLabel());
		
//		if(currentWindowMode == REVIEW_WINDOW_MODE.CREATE)
//		{
		add(new JLabel("Feature"));
		add(ddFeatureId);
		add(btnFeature);
		add(new JLabel());
//		}
		
		
		add(btnReset);
		add(btnSubmit);
		add(btnMainMenu);
		
		UserHandler handler = new UserHandler();
		if(currentWindowMode != REVIEW_WINDOW_MODE.CREATE)
		{
			ddReviewId.addActionListener(handler);
		}
		if(currentWindowMode == REVIEW_WINDOW_MODE.CREATE)
		{
			ddCarId.addActionListener(handler);
		}
		btnFeature.addActionListener(handler);
		btnReset.addActionListener(handler);
		btnSubmit.addActionListener(handler);
		btnMainMenu.addActionListener(handler);
	}
	
	private void AddFeature()
	{
		if(ddFeatureId.getSelectedIndex() > 0){
			String featId = (String)ddFeatureId.getSelectedItem();
			int numFeatId = Integer.parseInt(featId);
			if(featureList.contains(numFeatId) == false)
			{
				// not a duplicate
				featureList.add(numFeatId);
				ShowAddedFeature();
			}
			else
			{
				ShowFailedMsg("feature already added");
			}
		}
	}
	
	private void ShowAddedFeature()
	{
		String featureId = "";
		
		for(int index = 0; index < featureList.size(); index++)
		{
			featureId += String.valueOf( featureList.get(index));
			featureId += ", ";
		}
		
		lblMessage.setText(featureId);
		lblMessage.setForeground(Color.BLACK);
	}
	
 	private void BindReivew(Review review)
	{
		// only gets called when update/delete
		txtMemberId.setText(String.valueOf(review.memberId));
		txtCarId.setText(String.valueOf(review.carId));
		txtDescription.setText(review.description);
	}
	
	private void CreateReview()
	{
		Review review = GetReviewFromUI();
		
		if(review != null)
		{
			DB_Review db_review = new DB_Review();
			
			if(db_review.CreateReview(review))
			{
				ShowSuccessMsg("Review Created");
			}
			else
			{
				ShowFailedMsg("Can't create Review");
			}
		}
	}
	
	private void DeleteReview()
	{
		if(IsInputError() == false)
		{
			DB_Review db_review = new DB_Review();
			int review_id = Integer.parseInt((String) ddReviewId.getSelectedItem());
			if(db_review.DeleteReview(review_id))
			{
				ShowSuccessMsg("Delete Successfull");
			}
			else
			{
				ShowFailedMsg("Delete Failed");
			}
		}
	}
	
	private String[] GetAllCarId()
	{
		DB_Car db_car = new DB_Car();
		List<String> listCarId = db_car.GetAllCarId();
		listCarId.add(0, "-- Select Car Id --");
		String carIds[] = listCarId.toArray(new String[0]);
		return carIds;
	}
	
	private String[] GetAllMemberId()
	{
		DB_User db_user = new DB_User();
		List<String> listMemberId = db_user.GetAllMemberId();
		listMemberId.add(0, "-- Select Member Id --");
		String memberIds[] = listMemberId.toArray(new String[0]);
		return memberIds;
	}
	
	private String[] GetAllReviewId()
	{
		DB_Review db_review = new DB_Review();
		
		List<String> listReviewId = db_review.GetAllReviewId();
		listReviewId.add(0, "-- Select Review Id --");
		String reviewIds[] = listReviewId.toArray(new String[0]);
		return reviewIds;
	}
	
	private Review GetReviewById()
	{
		// only called when update/delete
		if(ddReviewId.getSelectedIndex() > 0)
		{
			// valid review id
			int review_id = Integer.parseInt((String) ddReviewId.getSelectedItem());
			DB_Review db_review = new DB_Review();
			Review review = db_review.GetReviewById(review_id);

			String features[] = GetFeatureByCarId(review.carId);
			ShowCarFeature(features);
			return review;
		}
		else
		{
			// no review selected
			return null;
		}
	}
	
	public Review GetReviewFromUI()
	{
		Review review = new Review();
		
		int carId;
		int memberId;
		
		if(IsInputError() == false)
		{
			// no input error
			if(currentWindowMode == REVIEW_WINDOW_MODE.CREATE)
			{
				carId = Integer.parseInt((String) ddCarId.getSelectedItem());
				memberId = Integer.parseInt((String) ddMemberId.getSelectedItem());
				
				review.carId = carId;
				review.memberId = memberId;
				review.description = txtDescription.getText();
				review.featureIdList = featureList;
			}
			else
			{
				// update/delete
				int reviewId = Integer.parseInt((String) ddReviewId.getSelectedItem());
				carId = Integer.parseInt(txtCarId.getText());
				memberId = Integer.parseInt(txtMemberId.getText());
				
				review.id = reviewId;
				review.carId = carId;
				review.memberId = memberId;
				review.description = txtDescription.getText();
				review.featureIdList = featureList;
			}
			return review;
		}
		else
		{
			return null;
		}
	}
	
	public String[] GetFeatureByCarId(int carId)
	{
		DB_Car db_car = new DB_Car();
		List<String> listFeatureId = db_car.GetFeatureByCarId(carId);
		if(listFeatureId.size() > 0)
			listFeatureId.add(0, "-- Select Feature Id --");
		else
			listFeatureId.add(0, "-- No Feature Id --");
		String featIds[] = listFeatureId.toArray(new String[0]);
		return featIds;

//		return null;
	}
	
	private void ShowCarFeature()
	{
		if(ddCarId.getSelectedIndex() > 0)
		{
			int carId = Integer.parseInt((String) ddCarId.getSelectedItem());
			String features[] = GetFeatureByCarId(carId);
			ShowCarFeature(features);
		}
	}
	
	private void ShowCarFeature(String features[])
	{
		modelFeatureList.removeAllElements();
		for(int i=0; i < features.length; i++)
		{
			modelFeatureList.addElement(features[i]);
		}
	}
	
	private boolean IsInputError()
	{
		boolean IsError = false;
		
		if(currentWindowMode == REVIEW_WINDOW_MODE.CREATE)
		{
			if(ddCarId.getSelectedIndex() < 1){
				IsError = true;
				ShowFailedMsg("Select a car");
			}
			if(ddMemberId.getSelectedIndex() < 1){
				IsError = true;
				ShowFailedMsg("Select Member Id");
			}
		}
		else
		{
			// update/delete
			if(ddReviewId.getSelectedIndex() < 1)
			{
				IsError = true;
				ShowFailedMsg("Select a Review");
			}
		}
		
		return IsError;
	}
	
	private void ShowSuccessMsg(String msg) {
		lblMessage.setText(msg);
		lblMessage.setForeground(Color.GREEN);
	}
	
	private void ShowFailedMsg(String msg) {
		lblMessage.setText(msg);
		lblMessage.setForeground(Color.RED);
	}
	
	private void ShowSelectedReview()
	{
		Review review = GetReviewById();
		if(review != null)
		{
			BindReivew(review);
		}
		else
		{
			ShowFailedMsg("Can't retrieve Review");
		}
	}
	
	private void UpdateReview()
	{
		Review review = GetReviewFromUI();
		if(review != null)
		{
			DB_Review db_review = new DB_Review();
			if(db_review.UpdateReviewById(review))
			{
				ShowSuccessMsg("Update Successful");
			}
			else
			{
				ShowFailedMsg("Update Failed");
			}
		}
		else
		{
			ShowFailedMsg("can't get review");
		}
	}
	
	private class UserHandler implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event) {
			// TODO Auto-generated method stub
			lblMessage.setText(null);
			
			if(currentWindowMode != REVIEW_WINDOW_MODE.CREATE && 
					event.getSource() == ddReviewId && ddReviewId.getSelectedIndex() > 0)
			{
				ShowSelectedReview();
			}
			
			if(currentWindowMode == REVIEW_WINDOW_MODE.CREATE && 
					event.getSource() == ddCarId && ddCarId.getSelectedIndex() > 0)
			{
				// update feature dropdown
				ShowCarFeature();
			}
			
			else if(event.getSource() == btnFeature)
			{
				AddFeature();
			}
			
			else if(event.getSource() == btnReset)
			{
				// reset all fields
			}
			
			else if(currentWindowMode == REVIEW_WINDOW_MODE.CREATE &&
					event.getSource() == btnSubmit)
			{
				// do sth
				CreateReview();
			}
			
			else if(currentWindowMode == REVIEW_WINDOW_MODE.UPDATE &&
						event.getSource() == btnSubmit)
			{
				UpdateReview();
			}
			
			else if (currentWindowMode == REVIEW_WINDOW_MODE.DELETE && 
						event.getSource() == btnSubmit)
			{
				DeleteReview();
			}
			else if(event.getSource() == btnMainMenu)
			{
				// goto main menu
				UI_Main_Menu mainMenu = new UI_Main_Menu();
				mainMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				mainMenu.setVisible(true);
				dispose();
			}
			
		}
		
	}
	
//	public static void main(String args[])
//	{
//		UI_Review review_window = new UI_Review(UI_Review.REVIEW_WINDOW_MODE.CREATE);
//		review_window.setVisible(true);
//		review_window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//	}
}
