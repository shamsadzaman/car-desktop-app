import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class UI_Connection extends JDialog implements ActionListener {
	/** Flag that shows if application should be closed (Cancel button) */
	boolean isCanceled = true;
	/** Database connection properties */
	private Properties props = null;
	/** Container */
	private Container container = null;
	/** Label for DB URL */
	private JLabel l_url = new JLabel("Database URL");
	/** Text field for DB URL */
	private JTextField tf_url = new JTextField();
	/** Label for Driver */
	private JLabel l_driver = new JLabel("Driver");
	/** Text field for Driver */
	private JTextField tf_driver = new JTextField();
	/** Label for Database */
	private JLabel l_db = new JLabel("Database");
	/** Text field for Database */
	private JTextField tf_db = new JTextField();
	/** Label for User Name */
	private JLabel l_user = new JLabel("User Name");
	/** Text field for User Name */
	private JTextField tf_user = new JTextField();
	/** Label for Password */
	private JLabel l_pswd = new JLabel("Password");
	/** Text field for Password */
	private JPasswordField tf_pswd = new JPasswordField();
	/** Label for Host Name */
	private JLabel l_host = new JLabel("Host Name");
	/** Text field for Host Name */
	private JTextField tf_host = new JTextField();
	/** Label for Port */
	private JLabel l_port = new JLabel("Port");
	/** Text field for Port */
	private JTextField tf_port = new JTextField();

	/** OK button */
	private JButton buttonOK = null;
	/** Cancel button */
	private JButton buttonCancel = null;

	/**
	 * Constructor
	 * 
	 * @param owner
	 *            owner of the window
	 * @param title
	 *            title of the window
	 * @param modal
	 *            flag that shows if window is modal
	 * @param p
	 *            connection properties
	 */
	public UI_Connection(JFrame owner, String title, boolean modal, Properties p) {
		// Call the JDialog constructor.
		super(owner, title, modal);

		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		int h = screenSize.height;
		int w = screenSize.width;
		setSize(w / 3, h / 4);
		setLocation(w / 3, h / 3);

		props = new Properties(p);
		setProps();

		container = this.getContentPane();

		// Create a confirmation button.
		buttonOK = new JButton("OK");
		buttonOK.setMnemonic('O');
		buttonOK.setPreferredSize(new Dimension(70, 25));
		buttonOK.addActionListener(this);

		// Create a cancel button.
		buttonCancel = new JButton("Cancel");
		buttonCancel.setMnemonic('C');
		buttonCancel.setPreferredSize(new Dimension(75, 25));
		buttonCancel.addActionListener(this);

		// Create a panel to hold the buttons
		JPanel butPanel = new JPanel();
		butPanel.add(buttonOK);
		butPanel.add(buttonCancel);

		// Use a grid layout
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayout(7, 2));
		mainPanel.add(l_url);
		mainPanel.add(tf_url);
		mainPanel.add(l_driver);
		mainPanel.add(tf_driver);
		mainPanel.add(l_db);
		mainPanel.add(tf_db);
		mainPanel.add(l_user);
		mainPanel.add(tf_user);
		mainPanel.add(l_pswd);
		mainPanel.add(tf_pswd);
		mainPanel.add(l_host);
		mainPanel.add(tf_host);
		mainPanel.add(l_port);
		mainPanel.add(tf_port);

		container.setLayout(new BorderLayout());
		container.add(mainPanel, BorderLayout.CENTER);
		container.add(butPanel, BorderLayout.SOUTH);
	}

	/**
	 * This method is invoked when button is clicked
	 * 
	 * @param e
	 *            action event
	 * @return None
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == buttonOK) {
			isCanceled = false;
		}

		// Close dialog after OK or Cancel button clicked
		this.dispose();
	}

	/**
	 * This method sets connection properties in a window (text fields)
	 * 
	 * @return None
	 */
	public void setProps() {
		tf_url.setText(props.getProperty("DATABASE_URL"));
		tf_driver.setText(props.getProperty("DRIVER"));
		tf_db.setText(props.getProperty("DATABASE"));
		tf_user.setText(props.getProperty("USER_NAME"));
		tf_host.setText(props.getProperty("HOST_NAME"));
		tf_port.setText(props.getProperty("PORT"));
	}

	/**
	 * This method gets connection properties from text fields
	 * 
	 * @return connection properties
	 */
	public Properties getProps() {
		props.put("DATABASE_URL", tf_url.getText());
		props.put("DRIVER", tf_driver.getText());
		props.put("DATABASE", tf_db.getText());
		props.put("USER_NAME", tf_user.getText());
		props.put("HOST_NAME", tf_host.getText());
		props.put("PORT", tf_port.getText());
		return props;
	}

	/**
	 * This method gets password from appropriate text field
	 * 
	 * @return password
	 */
	public String getPassword() {
		return new String(tf_pswd.getPassword());
	}

}
