import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class UI_Review_Rating extends JFrame
{
	
	private JLabel lblMessage;
	
	private JComboBox<String> ddReviewId;
	
	private JComboBox<String> ddMemberId;
	private JTextField txtMemberId;
	
	private JComboBox<String> ddRating;
	
	private JButton btnReset;
	private JButton btnSubmit;
	private JButton btnMainMenu;
	
	public static enum RATING_WINDOW_MODE
	{
		CREATE,
		UPDATE,
		DELETE
	}
	
	private RATING_WINDOW_MODE currentWindowMode;
	
	public UI_Review_Rating(RATING_WINDOW_MODE windowMode)
	{
		setTitle("Rating");
		currentWindowMode = windowMode;
		CreatreWindow();
	}
	
	private void CreatreWindow()
	{
		setLayout(new GridLayout(5, 4));
		setLocation(500, 300);
		setSize(500, 300);
		
		lblMessage = new JLabel();
		
		ddReviewId = new JComboBox<String>(GetAllReviewId());
		
//		if(currentWindowMode == RATING_WINDOW_MODE.CREATE)
//		{
			ddMemberId = new JComboBox<String>(GetAllMemberId());
			
//		}
//		else
//		{
//			txtMemberId = new JTextField(20);
//			txtMemberId.setEditable(false);
//		}
		
		ddRating = new JComboBox<String>(GetAllRatingId());
		
		if(currentWindowMode == RATING_WINDOW_MODE.DELETE)
			ddRating.setEnabled(false);
		
		btnReset = new JButton("Reset");
		
		String btnName = "Create Rating";
		if(currentWindowMode == RATING_WINDOW_MODE.UPDATE)
		{
			btnName = "Update Rating";
		}
		else if(currentWindowMode == RATING_WINDOW_MODE.DELETE)
		{
			btnName = "Delete Rating";
		}
		btnSubmit = new JButton(btnName);
		btnMainMenu = new JButton("Main Menu");
		
		add(lblMessage);
		add(new JLabel());
		add(new JLabel());
		add(new JLabel());
		
		add(new JLabel("Rater Id: "));
//		if(currentWindowMode == RATING_WINDOW_MODE.CREATE)
//		{
			add(ddMemberId);
//		}
//		else
//		{
//			add(txtMemberId);
//		}
		add(new JLabel());
		add(new JLabel());
		
		add(new JLabel("Review Id: "));
		add(ddReviewId);
		add(new JLabel());
		add(new JLabel());
		
		add(new JLabel("Rating: "));
		add(ddRating);
		add(new JLabel());
		add(new JLabel());
		
		add(btnReset);
		add(btnSubmit);
		add(btnMainMenu);
		
		UserHandler handler = new UserHandler();
		btnReset.addActionListener(handler);
		btnSubmit.addActionListener(handler);
		btnMainMenu.addActionListener(handler);
	}
	
	private void CreateRating()
	{
		Review review = GetRatingFromUI();
		
		if(review != null)
		{
			DB_Review db_review = new DB_Review();
			if(db_review.CreateRating(review))
			{
				ShowSuccessMsg("Rating Created");
			}
			else
			{
				ShowFailedMsg("Couldn't create rating");
			}
		}
		else
		{
			ShowFailedMsg("Couldn't get rating");
		}
	}
	
	private void DeleteRating()
	{
		int memId = Integer.parseInt((String) ddMemberId.getSelectedItem());
		int revId = Integer.parseInt((String) ddReviewId.getSelectedItem());
		
		DB_Review db_review = new DB_Review();
		
		if(db_review.DeleteRating(memId, revId))
		{
			ShowSuccessMsg("Rating Deleted");
		}
		else
		{
			ShowFailedMsg("Delete Failed");
		}
	}
	
	private String[] GetAllMemberId()
	{
		DB_User db_user = new DB_User();
		List<String> listMemberId = db_user.GetAllMemberId();
		listMemberId.add(0, "-- Select Member Id --");
		String memberIds[] = listMemberId.toArray(new String[0]);
		return memberIds;
	}
	
	private String[] GetAllRatingId()
	{
		DB_Review db_review = new DB_Review();
		
		List<String> listRatingId = db_review.GetAllRatingId();
		listRatingId.add(0, "-- Select Rating --");
		String ratings[] = listRatingId.toArray(new String[0]);
		return ratings;		
	}
	
	private String[] GetAllReviewId()
	{
		DB_Review db_review = new DB_Review();
		
		List<String> listReviewId = db_review.GetAllReviewId();
		listReviewId.add(0, "-- Select Review Id --");
		String reviewIds[] = listReviewId.toArray(new String[0]);
		return reviewIds;
	}
	
	private Review GetRatingFromUI()
	{
		Review review = new Review();
		
		int revId;
		int raterId;
		int rating;
		
		revId = Integer.parseInt((String) ddReviewId.getSelectedItem());
		
//		if(currentWindowMode == UI_Review_Rating.RATING_WINDOW_MODE.CREATE)
//		{
			raterId = Integer.parseInt((String) ddMemberId.getSelectedItem());
//		}
//		else
//		{
//			// update/delete
//			raterId = Integer.parseInt(txtMemberId.getText());
//		}
		
		rating = Integer.parseInt((String) ddRating.getSelectedItem());
		
		review.id = revId;
		review.memberId = raterId;
		review.rating = rating;
		
		return review;
	}
	
	private void ShowSuccessMsg(String msg) {
		lblMessage.setText(msg);
		lblMessage.setForeground(Color.GREEN);
	}
	
	private void ShowFailedMsg(String msg) {
		lblMessage.setText(msg);
		lblMessage.setForeground(Color.RED);
	}
	
	private void UpdateRating()
	{
		Review review = GetRatingFromUI();
		
		if(review != null)
		{
			DB_Review db_review = new DB_Review();
			if(db_review.UpdateRating(review))
			{
				ShowSuccessMsg("Update Successfull");
			}
			else
			{
				ShowFailedMsg("Update Failed");
			}
		}
	}
	
	private class UserHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			lblMessage.setText(null);
			
			if(event.getSource() == btnReset)
			{
				ddMemberId.setSelectedIndex(0);
				ddRating.setSelectedIndex(0);
				ddReviewId.setSelectedIndex(0);
			}
			
			else if(currentWindowMode == RATING_WINDOW_MODE.CREATE && 
					event.getSource() == btnSubmit)
			{
				CreateRating();
			}
			
			else if(currentWindowMode == RATING_WINDOW_MODE.UPDATE &&
					event.getSource() == btnSubmit)
			{
				UpdateRating();
			}
			
			else if(currentWindowMode == RATING_WINDOW_MODE.DELETE && 
					event.getSource() == btnSubmit)
			{
				DeleteRating();
			}
			
			else if(event.getSource() == btnMainMenu)
			{
				UI_Main_Menu mainMenu = new UI_Main_Menu();
				mainMenu.setVisible(true);
				mainMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose();
			}
		}
	}
	
//	public static void main(String args[])
//	{
//		UI_Review_Rating rrWindow = new UI_Review_Rating(
//				UI_Review_Rating.RATING_WINDOW_MODE.DELETE);
//		rrWindow.setVisible(true);
//		rrWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//	}
}
