import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;


public class DB_Query {

	private DatabaseConnection dbConnection;
	private DatabaseConnector dbConnector;

	public DB_Query() {
		// TODO Auto-generated constructor stub
		dbConnection = new DatabaseConnection();
		dbConnector = dbConnection.getConnector();
	}
	
	public ResultSet GetTodayReview(){
		Connection conn;
		CallableStatement cs;
		ResultSet rs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("begin SELECT_TODAY_REVIEW(?); end;",
					ResultSet.TYPE_SCROLL_SENSITIVE, 
                    ResultSet.CONCUR_READ_ONLY);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = ((OracleCallableStatement)cs).getCursor(1);
			return rs;
		}
		catch (Exception ex)
		{
			System.out.println("GetTodayReview Error: " + ex);
			return null;
		}
	}
	
	public ResultSet GetProcedureResult(String procName){
		Connection conn;
		CallableStatement cs;
		ResultSet rs;
		
		try{
			conn = dbConnector.getConnection();
////			conn = dbConnector.getConnection();
			cs = conn.prepareCall("begin " + procName + "(?); end;",
					ResultSet.TYPE_SCROLL_SENSITIVE, 
                    ResultSet.CONCUR_READ_ONLY);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = ((OracleCallableStatement)cs).getCursor(1);
			return rs;
		}
		catch (Exception ex)
		{
			System.out.println("GetTodayReview Error: " + ex);
			return null;
		}
	}
}
