import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * holds database connector
 * so you don't have to enter password
 * again and again
 * @author Shamsad
 * 
 */
public class DatabaseConnection {
	private static DatabaseConnector currentConector = null;

	// private static ConnectionProperties conProps = null;

	private String db_username;
	private String db_password;
	private String db_url;

	private Connection conn;

	public DatabaseConnection() 
	{
		// TODO Auto-generated constructor stub
		db_username = "mem_car_review";
		db_password = "123456";
		db_url = "jdbc:oracle:thin:@localhost:1521:sysdba";

		conn = null;
	}

	public Connection getStaticConnection() 
	{
		Properties connProps = new Properties();
		connProps.put("user", this.db_username);
		connProps.put("password", this.db_password);

		try {
			DriverManager.registerDriver((java.sql.Driver) Class.forName(
					"oracle.jdbc.driver.OracleDriver").newInstance());

			conn = DriverManager
					.getConnection(db_url, db_username, db_password);

			return conn;
		} catch (Exception ex) {
			System.out.println("Error: " + ex);
			return null;
		}

	}
	
	/**
	 * sets current database connector
	 * 
	 * @param dc
	 *            DatabaseConnector to set
	 */
	public void setConnector(DatabaseConnector dc) {
		currentConector = dc;
	}

	/**
	 * Returns current database connector
	 * 
	 * @return DatabaseConnector
	 */
	public DatabaseConnector getConnector() {
		return currentConector;
	}
	
	
}
