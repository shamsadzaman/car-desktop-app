import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;
import oracle.jdbc.OracleCallableStatement;
//import oracle.jdbc.OracleTypes;
//
//import oracle.jdbc.*;

public class DB_User {
	private DatabaseConnection dbConnection;
	private DatabaseConnector dbConnector;

	public DB_User() {
		// TODO Auto-generated constructor stub
		dbConnection = new DatabaseConnection();
		dbConnector = dbConnection.getConnector();
	}

	public boolean CreateUser(User user) {
		Connection conn;
		CallableStatement cs;

		try {
			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call insert_member(?, ?, ?, ?, ?, ?)}");
			cs.setString("pusername", user.username);
			cs.setString("pemail", user.email);
			cs.setInt("page", user.age);
			cs.setString("pgender", user.gender);
			cs.setString("pcity", user.city);
			cs.setString("pstate", user.state);
			cs.execute();
			return true;
		} catch (Exception ex) {
			System.out.println("Error: " + ex);
			return false;
		}
	}

	/*
	 * Delete user
	 */
	public boolean DeleteUser(User user) {
		Connection conn;
		CallableStatement cs;

		try {
			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call delete_member_by_id(?)}");
			cs.setInt("p_Id", user.id);
			cs.execute();
			return true;
		} catch (Exception ex) {
			System.out.println("Error: " + ex);
			return false;
		}
	}

	/*
	 * Get all the members
	 */
	public ResultSet GetAllUser() {
		Connection conn;
		CallableStatement cs;
		ResultSet rs;

		try {
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("begin select_all_member(?); end;");
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = ((OracleCallableStatement) cs).getCursor(1);
			return rs;
		} catch (Exception ex) {
			System.out.println("Error: " + ex);
			return null;
		}
	}

	public List<String> GetAllMemberId()
	{
		try{
			ResultSet rs = GetAllUser();
			
			List<String> listMemberId = new ArrayList<String>();
			
			while(rs.next()){
				int memberId = rs.getInt("Id");
				listMemberId.add(String.valueOf(memberId));
			}
//			String carNames[] = lstCarNames.toArray(new String[0]);
			return listMemberId;
		}
		catch(Exception ex)
		{
			System.out.println("GetAllMemberId error: " + ex);
			return null;
		}
	}
	
	/*
	 * Finds a member by email and returns all his info
	 */
	public User GetUserByEmail(String Email) {
		Connection conn;
		CallableStatement cs;
		User user = new User();

		try {
			conn = dbConnector.getConnection();
			cs = conn
					.prepareCall("{call select_member_by_email(?, ?, ?, ?, ?, ?, ?)}");
			cs.setString("p_Email", Email);
			cs.registerOutParameter("p_Id", OracleTypes.INTEGER);
			cs.registerOutParameter("p_Username", OracleTypes.VARCHAR);
			cs.registerOutParameter("p_Age", OracleTypes.INTEGER);
			cs.registerOutParameter("p_Gender", OracleTypes.VARCHAR);
			cs.registerOutParameter("p_City", OracleTypes.VARCHAR);
			cs.registerOutParameter("p_State", OracleTypes.VARCHAR);

			cs.execute();

			user.id = cs.getInt("p_Id");
			user.username = cs.getString("p_Username");
			user.email = Email;
			user.age = cs.getInt("p_Age");
			user.gender = cs.getString("p_Gender");
			user.city = cs.getString("p_City");
			user.state = cs.getString("p_State");

			return user;
		}

		catch (Exception ex) {
			System.out.println("Error: " + ex);
			return null;
		}
	}

	/*
	 * Finds member by username and returns all info
	 */
	public User GetUserByUserName(String username) {
		Connection conn;
		CallableStatement cs;
		User user = new User();

		try {
			conn = dbConnector.getConnection();
			cs = conn
					.prepareCall("{call select_member_by_username(?, ?, ?, ?, ?, ?, ?)}");
			cs.setString("p_Username", username);
			cs.registerOutParameter("p_Id", OracleTypes.INTEGER);
			cs.registerOutParameter("p_Email", OracleTypes.VARCHAR);
			cs.registerOutParameter("p_Age", OracleTypes.INTEGER);
			cs.registerOutParameter("p_Gender", OracleTypes.VARCHAR);
			cs.registerOutParameter("p_City", OracleTypes.VARCHAR);
			cs.registerOutParameter("p_State", OracleTypes.VARCHAR);

			cs.execute();

			user.id = cs.getInt("p_Id");
			user.username = username;
			user.email = cs.getString("p_Email");
			user.age = cs.getInt("p_Age");
			user.gender = cs.getString("p_Gender");
			user.city = cs.getString("p_City");
			user.state = cs.getString("p_State");

			return user;
		}

		catch (Exception ex) {
			System.out.println("Error: " + ex);
			return null;
		}

	}

	/*
	 * Update user using info in user
	 */
	public boolean UpdateUser(User user) {
		Connection conn;
		CallableStatement cs;

		try {
			conn = dbConnector.getConnection();
			cs = conn
					.prepareCall("{call update_member_by_id(?, ?, ?, ?, ?, ?, ?)}");
			cs.setInt("pId", user.id);
			cs.setString("pUsername", user.username);
			cs.setString("pEmail", user.email);
			cs.setInt("pAge", user.age);
			cs.setString("pGender", user.gender);
			cs.setString("pCity", user.city);
			cs.setString("pState", user.state);
			cs.execute();
			return true;
		} catch (Exception ex) {
			System.out.println("Error: " + ex);
			return false;
		}
	}
}
