import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class UI_Main_Menu extends JFrame 
{
	private JButton btnCreateDB;
	
	private JButton btnCreateUser;
	private JButton btnUpdateUser;
	private JButton btnDeleteUser;
	
	private JButton btnCreateCar;
	private JButton btnUpdateCar;
	private JButton btnDeleteCar;
	
	private JButton btnCreateReview;
	private JButton btnUpdateReview;
	private JButton btnDeleteReview;
	
	private JButton btnCreateRating;
	private JButton btnUpdateRating;
	private JButton btnDeleteRating;
	
	private JButton btnShowQuery;

	private JLabel lblMessage;

	public UI_Main_Menu() {
		// TODO Auto-generated constructor stub

		// super("List Test");
		setTitle("MAIN MENU");
		setSize(400, 150);
		setLocation(500, 250);
		// setLayout(new GridLayout(3, 1)); // set frame layout
		setLayout(new GridLayout(6, 3));

		btnCreateDB = new JButton("Create Database");
		btnCreateUser = new JButton("Create User");
		btnUpdateUser = new JButton("Update User");
		btnDeleteUser = new JButton("Delete User");
		
		btnCreateCar = new JButton("Create Car");
		btnUpdateCar = new JButton("Update Car");
		btnDeleteCar = new JButton("Delete Car");
		
		btnCreateReview = new JButton("Create Review");
		btnUpdateReview = new JButton("Update Review");
		btnDeleteReview = new JButton("Delete Review");

		btnCreateRating = new JButton("Create Rating");
		btnUpdateRating = new JButton("Update Rating");
		btnDeleteRating = new JButton("Delete Rating");
		
		btnShowQuery = new JButton("Show Query");
		
		lblMessage = new JLabel();

		add(lblMessage);
		add(btnCreateDB);
		add(new JLabel());
		
		add(btnCreateUser);
		add(btnUpdateUser);
		add(btnDeleteUser);
		
		add(btnCreateCar);
		add(btnUpdateCar);
		add(btnDeleteCar);
		
		add(btnCreateReview);
		add(btnUpdateReview);
		add(btnDeleteReview);
		
		add(btnCreateRating);
		add(btnUpdateRating);
		add(btnDeleteRating);
		
		add(new JLabel());
		add(btnShowQuery);
		add(new JLabel());

		Main_Menu_Btn_Handler handler = new Main_Menu_Btn_Handler();

		btnCreateDB.addActionListener(handler);
		btnCreateUser.addActionListener(handler);
		btnUpdateUser.addActionListener(handler);
		btnDeleteUser.addActionListener(handler);

		btnCreateCar.addActionListener(handler);
		btnUpdateCar.addActionListener(handler);
		btnDeleteCar.addActionListener(handler);
		
		btnCreateReview.addActionListener(handler);
		btnUpdateReview.addActionListener(handler);
		btnDeleteReview.addActionListener(handler);
		
		btnCreateRating.addActionListener(handler);
		btnUpdateRating.addActionListener(handler);
		btnDeleteRating.addActionListener(handler);
		
		btnShowQuery.addActionListener(handler);
	}

	private class Main_Menu_Btn_Handler implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == btnCreateDB) {
				DB_CreateDatabase db = new DB_CreateDatabase();
				if (db.CreateDB()) {
					lblMessage.setText("Database Successfully Created");
					lblMessage.setForeground(Color.GREEN);
				} else {
					lblMessage
							.setText("There was an error while creating Database; that's all we know :(");
					lblMessage.setForeground(Color.RED);
				}
			}
			if (event.getSource() == btnCreateUser) {
				UI_User userWindow = new UI_User(UI_User.WINDOW_MODE.CREATE);
				userWindow.setVisible(true);
				userWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose();
			} else if (event.getSource() == btnUpdateUser) {
				UI_User userWindow = new UI_User(UI_User.WINDOW_MODE.UPDATE);
				userWindow.setVisible(true);
				userWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose();
			} else if (event.getSource() == btnDeleteUser) {
				UI_User userWindow = new UI_User(UI_User.WINDOW_MODE.DELETE);
				userWindow.setVisible(true);
				userWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose();
			}
			else if(event.getSource() == btnCreateCar){
				UI_Car carWindow = new UI_Car(UI_Car.CAR_WINDOW_MODE.CREATE);
				carWindow.setVisible(true);
				carWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose();
			}
			else if(event.getSource() == btnUpdateCar){
				UI_Car carWindow = new UI_Car(UI_Car.CAR_WINDOW_MODE.UPDATE);
				carWindow.setVisible(true);
				carWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose();
			}
			else if(event.getSource() == btnDeleteCar){
				UI_Car carWindow = new UI_Car(UI_Car.CAR_WINDOW_MODE.DELETE);
				carWindow.setVisible(true);
				carWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose(); 
			}
			else if(event.getSource() == btnCreateReview){
				UI_Review review_Window = new UI_Review(UI_Review.REVIEW_WINDOW_MODE.CREATE);
				review_Window.setVisible(true);
				review_Window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose(); 
			}
			else if(event.getSource() == btnUpdateReview){
				UI_Review review_Window = new UI_Review(UI_Review.REVIEW_WINDOW_MODE.UPDATE);
				review_Window.setVisible(true);
				review_Window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose(); 
			}
			else if(event.getSource() == btnDeleteReview){
				UI_Review review_Window = new UI_Review(UI_Review.REVIEW_WINDOW_MODE.DELETE);
				review_Window.setVisible(true);
				review_Window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose(); 
			}
			else if(event.getSource() == btnCreateRating)
			{
				UI_Review_Rating ratingWindow = new UI_Review_Rating(
						UI_Review_Rating.RATING_WINDOW_MODE.CREATE);
				ratingWindow.setVisible(true);
				ratingWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose();
			}
			else if(event.getSource() == btnUpdateRating)
			{
				UI_Review_Rating ratingWindow = new UI_Review_Rating(
						UI_Review_Rating.RATING_WINDOW_MODE.UPDATE);
				ratingWindow.setVisible(true);
				ratingWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose();
			}
			else if(event.getSource() == btnDeleteRating)
			{
				UI_Review_Rating ratingWindow = new UI_Review_Rating(
						UI_Review_Rating.RATING_WINDOW_MODE.DELETE);
				ratingWindow.setVisible(true);
				ratingWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose();
			}
			else if(event.getSource() == btnShowQuery)
			{
				UI_TableData queryWindow = new UI_TableData();
				queryWindow.setVisible(true);
				queryWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				dispose();
			}

			// JOptionPane.showMessageDialog(null, string +
			// " button is clicked");
		}
	}
}
