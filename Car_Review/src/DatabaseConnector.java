import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * DatabaseConnector - provides convenient interface for connecting to DB and
 * working with database through JDBC
 * */
class DatabaseConnector {
	/** Connection object */
	private static Connection conn = null;
	/** Statement object */
	private Statement stat = null;

	/** the url of the database */
	public static String url; // the url of the database
	/** the name of the database */
	private static String database; // the name of the database
	/** your username */
	public static String username; // your username
	/** your password */
	public static String password; // your password
	/** host name */
	private static String hostname;
	/** the port number to connect */
	private static String port; // the port number to connect
	/** driver for connection */
	private static String driver;

	/**
	 * Constructor
	 * 
	 * @param props
	 *            connection properties
	 * @param pswd
	 *            password for connection
	 */
	public DatabaseConnector(Properties props, String pswd) {
		url = props.getProperty("DATABASE_URL");
		database = props.getProperty("DATABASE");
		username = props.getProperty("USER_NAME");
		password = pswd;
		hostname = props.getProperty("HOST_NAME");
		port = props.getProperty("PORT");
		driver = props.getProperty("DRIVER");

		url = url + hostname + ":" + port + ":" + database;
	}

	/**
	 * Method establishes a connection
	 * 
	 * @return Flag if connection is established successfully
	 */
	public boolean open() {
		try {
			DriverManager.registerDriver((java.sql.Driver) Class
					.forName(driver).newInstance());
			conn = DriverManager.getConnection(url, username, password);
			// conn.setAutoCommit(false);
			stat = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);

		} catch (Exception ex) {

			System.out.println("Error: " + ex);
			if (conn == null) {
				return false;
			}
		}

		System.out.println("Connection with database server " + url
				+ " is established successfully.");
		return true;
	}

	/** Method closes a connection */
	public void close() {
		try {
			if (stat != null) {
				stat.close();
			}

			if (conn != null) {
				conn.close();
			}
		} catch (SQLException ex) {
			System.out.println("Error: " + ex);
			return;
		}

		System.out.println("Connection is closed successfully.");
	}

	/**
	 * Method returns status of a connection
	 * 
	 * @return status of a connection
	 */
	public boolean isConnected() {
		try {
			if (conn != null && !conn.isClosed()) {
				return true;
			}
		} catch (SQLException ex) {

		}
		return false;
	}

	public Connection getConnection() {
		try {
			// i
			if (conn == null || conn.isClosed())
				open();

			return conn;
		} catch (Exception ex) {
			System.out.println("Error: " + ex);
		}
		return null;
	}

	/**
	 * Method executes SQL statement
	 * 
	 * @return status of a command (e.g. number of rows inserted)
	 */
	public int executeUpdate(String s) throws SQLException {
		return stat.executeUpdate(s);
	}

	/**
	 * Method executes SQL query
	 * 
	 * @return set of rows
	 */
	public ResultSet executeQuery(String s) throws SQLException {
		return stat.executeQuery(s);
	}

	/** Method set Auto Commit flag for connection */
	public void setAutoCommit(boolean b) {
		try {
			conn.setAutoCommit(b);
		} catch (Exception ex) {
		}
	}

	/** Method rollbacks transaction */
	public void rollback() {
		try {
			conn.rollback();
		} catch (Exception ex) {
		}
	}

	/** Method commits transaction */
	public void commit() {
		try {
			conn.commit();
		} catch (Exception ex) {
		}
	}
} // finish class DatabaseConnector
