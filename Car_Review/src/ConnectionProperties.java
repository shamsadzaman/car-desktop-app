import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * ConnectionProperties - class incapsulates database connection properties
 * */
class ConnectionProperties {
	/** Contains connection properties */
	Properties props = null;

	/** Constructor */
	public ConnectionProperties() {
		try {
			props = new Properties();
			FileInputStream in = new FileInputStream("database.ini");
			props.load(in);
			in.close();
		} catch (FileNotFoundException ex) {
			// Setting default properties
			// JOptionPane.showMessageDialog(null,
			// "File with database connection settings was not found. Default settings will be used.",
			// "Warning", JOptionPane.WARNING_MESSAGE);
			props.put("DATABASE_URL", "jdbc:oracle:thin:@");
			props.put("DRIVER", "oracle.jdbc.driver.OracleDriver");
			props.put("DATABASE", "test");
			props.put("USER_NAME", "CSC6710_");
			props.put("HOST_NAME", "");
			props.put("PORT", "1521");
			System.out.println("Warning: " + ex);
			return;
		} catch (SecurityException ex) {
			System.out.println("Warning: " + ex);
			return;
		} catch (IOException ex) {
			System.out.println("Warning: " + ex);
			return;
		}
	}

	/**
	 * Constructor
	 * 
	 * @param p
	 *            connection properties
	 */
	public ConnectionProperties(Properties p) {
		try {
			props = new Properties(p);
			FileOutputStream out = new FileOutputStream("database.ini");
			p.store(out, "Database connection settings");
		} catch (Exception ex) {
			System.out.println("Warning: " + ex);
			return;
		}
	}
} // finish class ConnectionProperties