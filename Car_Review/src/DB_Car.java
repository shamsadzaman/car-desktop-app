import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;


public class DB_Car {

	private DatabaseConnection dbConnection;
	private DatabaseConnector dbConnector;

	public DB_Car() {
		// TODO Auto-generated constructor stub
		dbConnection = new DatabaseConnection();
		dbConnector = dbConnection.getConnector();
	}

	public boolean CreateCar(Car car)
	{
		Connection conn;
		CallableStatement cs;
		int carId = 0;
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call insert_car(?, ?, ?, ?, ?)}");
			cs.setString("p_Manufacturer", car.manufacturer);
			cs.setString("p_Make", car.make);
			cs.setInt("p_Year", car.year);
			cs.setString("p_Model", car.model);
			cs.registerOutParameter("o_car_id", OracleTypes.INTEGER);
			cs.execute();
			
			carId = cs.getInt("o_car_id");
			if(car.featureId != null && car.featureId.size() > 0)
			{
				AddFeature(carId, car.featureId);
			}
			return true;
		}
		catch(Exception ex)
		{
			System.out.println("Error: " + ex);
			return false;
		}		
	}

	public void AddFeature(int carId, List<Integer> featureList)
	{
		Connection conn;
		CallableStatement cs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call insert_car_feature(?, ?)}");
			
			for(int index = 0; index < featureList.size(); index++){
				cs.setInt("p_car_id", carId);
				cs.setInt("p_feature_id", featureList.get(index));
				cs.execute();
			}
		}
		catch(Exception ex)
		{
			System.out.println("AddFeature Error: " + ex);
			return ;
		}
	}
	
	public boolean DeleteCar(Car car){
		Connection conn;
		CallableStatement cs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call delete_car_by_id(?)}");
			cs.setInt("p_id", car.id);
			cs.execute();
			
			return true;
		}
		catch(Exception ex){
			System.out.println("DeleteCar Error: " + ex);
			return false;
		}
	}
	
	public ResultSet GetAllCar(){
		Connection conn;
		CallableStatement cs;
		ResultSet rs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("begin select_all_car(?); end;",
					ResultSet.TYPE_SCROLL_SENSITIVE, 
                    ResultSet.CONCUR_READ_ONLY);
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = ((OracleCallableStatement)cs).getCursor(1);
			return rs;
		}
		catch (Exception ex)
		{
			System.out.println("Get All Car Error: " + ex);
			return null;
		}
	}
	
	public List<String> GetAllCarId()
	{
		try{
			ResultSet rs = GetAllCar();
			
			List<String> listCarId = new ArrayList<String>();
			
			while(rs.next()){
				int carId = rs.getInt("Id");
				listCarId.add(String.valueOf(carId));
			}
//			String carNames[] = lstCarNames.toArray(new String[0]);
			return listCarId;
		}
		catch(Exception ex)
		{
			System.out.println("GetAllCarId error: " + ex);
			return null;
		}
	}

	public Car GetCarById(int carId){
		Connection conn;
		CallableStatement cs;
		Car car = new Car();
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call select_car_by_id(?, ?, ?, ?, ?)");
			cs.setInt("p_id", carId);
			cs.registerOutParameter("p_manufacturer", OracleTypes.VARCHAR);
			cs.registerOutParameter("p_make", OracleTypes.VARCHAR);
			cs.registerOutParameter("p_year", OracleTypes.INTEGER);
			cs.registerOutParameter("p_model", OracleTypes.VARCHAR);
			
			cs.execute();
			
			car.id = carId;
			car.manufacturer = cs.getString("p_manufacturer");
			car.make = cs.getString("p_make");
			car.year = cs.getInt("p_year");
			car.model = cs.getString("p_model");
			
			return car;
		}
		catch(Exception ex){
			System.out.println("SelectCarByID Error: " + ex);
			return null;
		}
	}

	private ResultSet GetAllFeature()
	{
		Connection conn;
		CallableStatement cs;
		ResultSet rs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("begin select_all_feature(?); end;");
			cs.registerOutParameter(1, OracleTypes.CURSOR);
			cs.execute();
			rs = ((OracleCallableStatement)cs).getCursor(1);
			return rs;
		}
		catch (Exception ex)
		{
			System.out.println("Get All Feature Error: " + ex);
			return null;
		}
	}

	public List<String> GetAllFeatureId()
	{
		try{
			ResultSet rs = GetAllFeature();
			
			List<String> listFeatureId = new ArrayList<String>();
			
			while(rs.next()){
				int carId = rs.getInt("Id");
				listFeatureId.add(String.valueOf(carId));
			}
//			String carNames[] = lstCarNames.toArray(new String[0]);
			return listFeatureId;
		}
		catch(Exception ex)
		{
			System.out.println("GetAllCarId error: " + ex);
			return null;
		}
	}

	private ResultSet GetFeatureIdByCarId(int carId)
	{
		Connection conn;
		CallableStatement cs;
		ResultSet rs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("begin SELECT_FEATURE_BY_CAR_ID(?, ?); end;");
			cs.setInt(1, carId);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			cs.execute();
			rs = ((OracleCallableStatement)cs).getCursor(2);
			return rs;
		}
		catch (Exception ex)
		{
			System.out.println("Get All Feature Error: " + ex);
			return null;
		}
	}
	
	public List<String> GetFeatureByCarId(int carId)
	{
		try{
			ResultSet rs = GetFeatureIdByCarId(carId);
			
			List<String> listFeatureId = new ArrayList<String>();
			
			while(rs.next()){
				int featureId = rs.getInt("feature_Id");
				listFeatureId.add(String.valueOf(featureId));
			}
//			String carNames[] = lstCarNames.toArray(new String[0]);
			return listFeatureId;
		}
		catch(Exception ex)
		{
			System.out.println("GetAllCarId error: " + ex);
			return null;
		}
	}
	
	public boolean UpdateCar(Car car){
		Connection conn;
		CallableStatement cs;
		
		try{
			conn = dbConnector.getConnection();
//			conn = dbConnector.getConnection();
			cs = conn.prepareCall("{call update_car_by_id(?, ?, ?, ?, ?)}");
			cs.setInt("p_id", car.id);
			cs.setString("p_manufacturer", car.manufacturer);
			cs.setString("p_make", car.make);
			cs.setInt("p_year", car.year);
			cs.setString("p_model", car.model);
			cs.execute();

			return true;
		}
		catch (Exception ex){
			System.out.println("UpdateCar Error: " + ex);
			return false;
		}
	}

}
