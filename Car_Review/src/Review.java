import java.util.ArrayList;
import java.util.List;


public class Review {
	public int id;
	public int carId;
	public int memberId;
	public String description;
	public List<Integer> featureIdList;
	public int rating;
	
	public Review(){
		featureIdList = new ArrayList<Integer>();
	}
}
