import java.util.ArrayList;
import java.util.List;


public class Car {
	public int id;
	public String manufacturer;
	public String make;
	public int year;
	public String model;
	public List<Integer> featureId;
	
	public Car()
	{
		featureId = new ArrayList<Integer>();
	}
}
