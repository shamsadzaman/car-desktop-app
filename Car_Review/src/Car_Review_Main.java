import java.sql.ResultSet;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Car_Review_Main {

	private static ConnectionProperties currentProps = null;
	private static DatabaseConnector dc = null;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		DB_Car_Review db = new DB_Car_Review();
		// try{
		// Class.forName("oracle.jdbc.driver.OracleDriver");
		// }
		// catch(ClassNotFoundException e){
		// System.out.println("JDBC not found");
		// e.printStackTrace();
		// return;
		// }
		// try {
		// db.ConnectToDB();
		// ResultSet rs = db.GetAllCar();
		// db.PrintRecord(rs);
		// } catch (SQLException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// List<String> lstCarNames = db.GetAllCarName();
		// // int size = lstCarNames.size();
		// String carNames[] = lstCarNames.toArray(new String[0]);
		// // for(int i=0; i < size; i++)
		// // {
		// // System.out.println(carNames[i]);
		// // }
		// UI_Car_Review ui = new UI_Car_Review(carNames);
		// ui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// ui.setSize(350, 150);
		// ui.setVisible(true);

		// UI_Functions ui = new UI_Functions();
		// ui.ShowMainMenu();

		Car_Review_Main car_review = new Car_Review_Main();
		car_review.ShowMainMenu();
		// car_review.ShowUserMenu();
		// car_review.ShowUser();
		// car_review.UpdateUser();

		// car_review.CreateUser();
		ConnectionProperties props = new ConnectionProperties();

		JFrame testFrame = new JFrame();
		UI_Connection connectDiag = new UI_Connection(testFrame, "test", true,
				props.props);
		connectDiag.setVisible(true);

		if (connectDiag.isCanceled) {
			System.exit(0);
		}

		// get the connection information from UI
		// and save it in currentProps
		currentProps = new ConnectionProperties(connectDiag.getProps());
		
		// provide DatabaseConnector necessary info to connect to DB
		dc = new DatabaseConnector(currentProps.props,
				connectDiag.getPassword());

		// DatabaseConnecion stores the connector throughout the application
		// so user doesn't have to connect again
		DatabaseConnection dbConn = new DatabaseConnection();
		dbConn.setConnector(dc);

		if (!dc.open()) {
			// couldn't make connection
			JOptionPane.showMessageDialog(
							testFrame,
							"Can not connect to the database. The application will be closed.",
							"Fatal error", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		} 
		else {
			// connection made to database
			JOptionPane.showMessageDialog(testFrame, "Connection made",
					"Success", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public void ShowMainMenu() {
		UI_Main_Menu mainMenu = new UI_Main_Menu();
		mainMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		mainMenu.setSize(350, 150);
		mainMenu.setVisible(true);
	}

//	public void ShowUserMenu() {
//		UI_User userWindow = new UI_User(UI_User.WINDOW_MODE.CREATE);
//		userWindow.setLocation(300, 300);
//		// userWindow.setSize(300, 300);
//		userWindow.setVisible(true);
//		userWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//	}
//
//	public void ShowCarInList() {
//		DB_Car_Review db = new DB_Car_Review();
//		List<String> lstCarNames = db.GetAllCarName();
//
//		String carNames[] = lstCarNames.toArray(new String[0]);
//		UI_Car_Review ui = new UI_Car_Review(carNames);
//		ui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		ui.setSize(350, 150);
//		ui.setVisible(true);
//	}
//
//	public void CreateUser() {
//		User user = new User();
//		user.username = "thor";
//		user.email = "thor@yahoo.com";
//		user.age = 35;
//		user.gender = "male";
//		user.city = "Narnia";
//		user.state = "OH";
//
//		DB_User db_user = new DB_User();
//		if (db_user.CreateUser(user)) {
//			System.out.println("User Created");
//		} else {
//			System.out.println("Couldn't create user");
//		}
//
//	}
//
//	public void UpdateUser() {
//		User user = new User();
//		user.id = 8;
//		user.username = "twitch";
//		user.email = "temo@tent.com";
//		user.age = 18;
//		user.gender = "male";
//		user.city = "batcave";
//		user.state = "OH";
//
//		DB_User db_user = new DB_User();
//		if (db_user.UpdateUser(user)) {
//			System.out.println("User updated");
//		} else {
//			System.out.println("Update Failed");
//		}
//	}
//
//	public User GetFakeUser() {
//		User user = new User();
//		user.username = "thor";
//		user.email = "thor@yahoo.com";
//		user.age = 35;
//		user.gender = "male";
//		user.city = "Narnia";
//		user.state = "OH";
//
//		return user;
//	}
//
//	public void ShowUser() {
//		User user = new User();
//		user.username = "thor";
//		user.email = "thor@yahoo.com";
//		user.age = 35;
//		user.gender = "male";
//		user.city = "Narnia";
//		user.state = "OH";
//
//		UI_User userWindow = new UI_User(UI_User.WINDOW_MODE.CREATE);
//		userWindow.setVisible(true);
//		// userWindow.test(user);
//
//	}

}
