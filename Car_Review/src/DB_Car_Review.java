import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

//import oracle.jdbc.oracore.OracleType;

import oracle.jdbc.*;

//import java.lang.*;

public class DB_Car_Review {

	private String db_username;
	private String db_password;
	private String db_url;

	private Connection conn;

	public DB_Car_Review() 
	{
		// TODO Auto-generated constructor stub
		db_username = "test_car_review";
		db_password = "123456";
		db_url = "jdbc:oracle:thin:@localhost:1521:sysdba";

		conn = null;
	}

	public Connection ConnectToDB() 
	{
		Properties connProps = new Properties();
		connProps.put("user", this.db_username);
		connProps.put("password", this.db_password);

		try {
			DriverManager.registerDriver((java.sql.Driver) Class.forName(
					"oracle.jdbc.driver.OracleDriver").newInstance());

			conn = DriverManager
					.getConnection(db_url, db_username, db_password);

			return conn;
		} catch (Exception ex) {
			System.out.println("Error: " + ex);
			return null;
		}

	}

	public ResultSet GetAllCar() 
	{
		try {
			if (conn == null) {
				ConnectToDB();
			}
			CallableStatement cs = this.conn
					.prepareCall("begin select_all_car(?); end;");

			cs.registerOutParameter(1, OracleTypes.CURSOR); // ref cursor
			cs.execute();
			ResultSet rs = ((OracleCallableStatement) cs).getCursor(1);
			return rs;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public List<String> GetAllCarName() 
	{
		// int abc = 10;

		try {
			ResultSet rs = GetAllCar();
			// int size = rs.getFetchSize();
			// String allCarName[] = new String[size];
			List<String> listCarName = new ArrayList<String>();

			// int i = 0;
			while (rs.next()) {
				listCarName.add(rs.getString("Make"));
				// allCarName[i++] = rs.getString("Make");
			}
			return listCarName;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	public void PrintRecord(ResultSet rs) {

		ResultSetMetaData metaData;
		int numColumn;

		try {
			// rs = GetAllCar();
			metaData = rs.getMetaData();

			numColumn = metaData.getColumnCount();

			for (int i = 1; i <= numColumn; i++) {
				System.out.printf("%-8s\t", metaData.getColumnName(i));
			}
			System.out.println();

			while (rs.next()) {
				for (int i = 1; i <= numColumn; i++) {
					System.out.printf("%-8s\t", rs.getObject(i));
					// System.out.println(rs.getInt("Id") + " : " +
					// rs.getString("Manufacturer") + " : " +
					// rs.getString("Make") + " : " + rs.getInt("Year") + " : "
					// + rs.getString("Model"));

				}
				System.out.println();

			} // end of while

			rs.close();
			rs = null;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	} // end of PrintRecord
}
