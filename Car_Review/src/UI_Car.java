import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class UI_Car extends JFrame{

	private JLabel lblMessage;
	private String lblInfoSuccess;
	private String lblInfoFailed;
	
	private JComboBox ddCarId;
	
	private JLabel lblManufacturer;
	private JTextField txtManufacturer;
	
	private JLabel lblMake;
	private JTextField txtMake;
	
	private JLabel lblYear;
	private JTextField txtYear;
	
	private JLabel lblModel;
	private JTextField txtModel;
	
	private JLabel lblFeatureId;
	private JComboBox ddFeatureId;
	private JButton btnFeature;
	
	private JButton btnReset;
	private JButton btnSubmit;
	private JButton btnMainMenu;
	
	public static enum CAR_WINDOW_MODE {
		CREATE, UPDATE, DELETE
	};
	
	private CAR_WINDOW_MODE currentWindowMode;

	List<Integer> featureList;
	
	public UI_Car(CAR_WINDOW_MODE windowMode)
	{
		setTitle("Car");
		currentWindowMode = windowMode;
		featureList = new ArrayList<Integer>();
		
		CreateWindow();
	}

	private void CreateWindow()
	{
		setLayout(new GridLayout(7, 4));
		setLocation(500, 300);
		setSize(500, 300);
		
		
		
//		db_car = new DB_Car();
		
		lblMessage = new JLabel("Messaege");
		
		if(currentWindowMode != CAR_WINDOW_MODE.CREATE){
			ddCarId = new JComboBox<String>(GetAllCarId());
		}
		
		lblManufacturer = new JLabel("Manufacturer: ");
		txtManufacturer = new JTextField(20);
		
		lblMake = new JLabel("Make: ");
		txtMake = new JTextField(20);
		
		lblYear = new JLabel("Year: ");
		txtYear = new JTextField(20);
		
		lblModel = new JLabel("Model: ");
		txtModel = new JTextField(20);
		
		if(currentWindowMode == CAR_WINDOW_MODE.CREATE){
			lblFeatureId = new JLabel("Add Feature");
			ddFeatureId = new JComboBox<String>(GetAllFeatureId());
			btnFeature = new JButton("Add Feature");
		}
		
		btnReset = new JButton("Reset");
		
		String buttonText = "Create Car";	// default
		
		if(currentWindowMode == CAR_WINDOW_MODE.UPDATE){
			buttonText = "Update Car";
		}
		else if(currentWindowMode == CAR_WINDOW_MODE.DELETE){
			buttonText = "Delete Car";
		}
		btnSubmit = new JButton(buttonText);
		btnMainMenu = new JButton("Main Menu");
		
		add(lblMessage);
		if(currentWindowMode != CAR_WINDOW_MODE.CREATE){
			// add dropdown to select car id for update/delete
			add(ddCarId);
		}
		else{
			// empty for create car operation
			add(new JLabel());
		}
		add(new JLabel("instruction"));
		add(new JLabel());
		
		add(lblManufacturer);
		add(txtManufacturer);
		add(new JLabel());
		add(new JLabel());
		
		add(lblMake);
		add(txtMake);
		add(new JLabel());
		add(new JLabel());
		
		add(lblYear);
		add(txtYear);
		add(new JLabel());
		add(new JLabel());
		
		add(lblModel);
		add(txtModel);
		add(new JLabel());
		add(new JLabel());
		
		if(currentWindowMode == CAR_WINDOW_MODE.CREATE){
			add(lblFeatureId);
			add(ddFeatureId);
			add(btnFeature);
			add(new JLabel("Feature gets added in background"));
		}
		else{
			add(new JLabel());
			add(new JLabel());
			add(new JLabel());
			add(new JLabel());
		}
		
		add(btnReset);
		add(btnSubmit);
		add(btnMainMenu);
		
		UserHandler handler = new UserHandler();
		if(currentWindowMode != CAR_WINDOW_MODE.CREATE){
			ddCarId.addActionListener(handler);
		}
		if(currentWindowMode == CAR_WINDOW_MODE.CREATE){
			btnFeature.addActionListener(handler);
		}
		btnReset.addActionListener(handler);
		btnSubmit.addActionListener(handler);
		btnMainMenu.addActionListener(handler);
	}
	
//	private void ShowSuccessMsg() {
//		lblMessage.setText(lblInfoSuccess);
//		lblMessage.setForeground(Color.GREEN);
//	}
	
	private void ShowSuccessMsg(String msg) {
		lblMessage.setText(msg);
		lblMessage.setForeground(Color.GREEN);
	}
	
//	private void ShowFailedMsg() {
//		lblMessage.setText(lblInfoFailed);
//		lblMessage.setForeground(Color.RED);
//	}
	
	private void AddFeature()
	{
		if(ddFeatureId.getSelectedIndex() > 0)
		{
			String featId = (String)ddFeatureId.getSelectedItem();
			int numFeatId = Integer.parseInt(featId);
			if(featureList.contains(numFeatId) == false)
			{
				// not a duplicate
				featureList.add(numFeatId);
				ShowAddedFeature();
			}
			else
			{
				ShowFailedMsg("feature already added");
			}
		}
	}
	
	private void ShowAddedFeature()
	{
		String featureId = "";
		
		for(int index = 0; index < featureList.size(); index++)
		{
			featureId += String.valueOf( featureList.get(index));
			featureId += ", ";
		}
		
		lblMessage.setText(featureId);
		lblMessage.setForeground(Color.BLACK);
	}
	
	private void BindCarData(Car car){
		txtManufacturer.setText(car.manufacturer);
		txtMake.setText(car.make);
		txtYear.setText(String.valueOf(car.year));
		txtModel.setText(car.model);
	}
	
	private void ShowFailedMsg(String msg) {
		lblMessage.setText(msg);
		lblMessage.setForeground(Color.RED);
	}
	
	private String[] GetAllCarId()
	{
		DB_Car db_car = new DB_Car();
		List<String> listCarId = db_car.GetAllCarId();
		listCarId.add(0, "-- Select Car Id --");
		String carIds[] = listCarId.toArray(new String[0]);
		return carIds;
	}
	
	private String[] GetAllFeatureId()
	{
		DB_Car db_car = new DB_Car();
		List<String> listFeatId = db_car.GetAllFeatureId();
		listFeatId.add(0, "--Select Feat Id--");
		String featIds[] = listFeatId.toArray(new String[0]);
		return featIds;		
	}
	
	/**
	 * Gets car info from UI
	 * @return car info from UI
	 */
	private Car GetCarInfoFromUI()
	{
		Car car = new Car();
		if(currentWindowMode != CAR_WINDOW_MODE.CREATE && 
				ddCarId.getSelectedIndex() > 0){
			car.id = Integer.parseInt(String.valueOf(ddCarId.getSelectedItem()));
		}
			
		car.manufacturer = txtManufacturer.getText();
		car.make = txtMake.getText();
		if(txtYear.getText().length() > 0){
			car.year = Integer.parseInt(txtYear.getText());
		}
		else{
			// year empty, put some value
			car.year = -1;
		}
		car.model = txtModel.getText();
		car.featureId = featureList;
		return car;
	}
	
	private void ShowCarInfo()
	{
		DB_Car db_car = new DB_Car();
		Car car = new Car();
		int carId = Integer.parseInt(String.valueOf(ddCarId.getSelectedItem()));
		
		car = db_car.GetCarById(carId);
		if(car != null){
			BindCarData(car);
		}
		else{
			ShowFailedMsg("Can't Get Car Info");
		}
	}
	
	private void ResetFields(){
		if(currentWindowMode != CAR_WINDOW_MODE.CREATE){
			ddCarId.setSelectedIndex(0);
		}
		txtManufacturer.setText(null);
		txtMake.setText(null);
		txtYear.setText(null);
		txtModel.setText(null);
	}
	
	private void CreateCar()
	{
		DB_Car db_car = new DB_Car();
		
		Car car = GetCarInfoFromUI();
		
		if(db_car.CreateCar(car)){
			ShowSuccessMsg("Create Successfull");
		}
		else{
			ShowFailedMsg("Create Failed");
		}
		
	}
	
	private void DeleteCar(){
		DB_Car db_car = new DB_Car();
		Car car = GetCarInfoFromUI();
		
		if(car != null){
			if(db_car.DeleteCar(car)){
				ShowSuccessMsg("Delete Successfull");
			}
			else{
				ShowFailedMsg("Delete Failed");
			}
		}
		else{
			ShowFailedMsg("Car info problem");
		}
	}
	
	private void UpdateCar(){
		Car car = GetCarInfoFromUI();
		DB_Car db_car = new DB_Car();
		
		if(car != null){
			if(db_car.UpdateCar(car)){
				ShowSuccessMsg("Update Successfull");
			}
			else{
				ShowFailedMsg("Updated Failed");
			}
		}
		else{
			ShowFailedMsg("Car Info Problem");
		}
	}
	
	private class UserHandler implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent event) {
			// TODO Auto-generated method stub
			lblMessage.setText(null);
			
			if(event.getSource() == ddCarId && ddCarId.getSelectedIndex() > 0){
				// car id selected
				ShowCarInfo();
			}
			
			else if(currentWindowMode == CAR_WINDOW_MODE.CREATE &&
					event.getSource() == btnFeature){
				AddFeature();
			}
			
			else if(event.getSource() == btnReset){
				ResetFields();
			}
			
			else if(event.getSource() == btnMainMenu){
				UI_Main_Menu mainMenu = new UI_Main_Menu();
				mainMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				mainMenu.setVisible(true);
				dispose();
			}
			
			else if(currentWindowMode == CAR_WINDOW_MODE.CREATE &&
					event.getSource() == btnSubmit){
				CreateCar();
			}
			
			else if(currentWindowMode == CAR_WINDOW_MODE.UPDATE &&
					event.getSource() == btnSubmit){
				UpdateCar();
			}
			
			else if(currentWindowMode == CAR_WINDOW_MODE.DELETE &&
					event.getSource() == btnSubmit){
				DeleteCar();
			}
		}
	}
	
//	public static void main(String[] args)
//	{
//		UI_Car carWindow = new UI_Car(UI_Car.CAR_WINDOW_MODE.CREATE);
//		carWindow.setVisible(true);
//		carWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//	}
}
