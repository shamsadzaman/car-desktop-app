

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class UI_TableData extends JFrame {
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JTextArea results = null;
	private JComboBox<String> ddQuery;
	private JButton btnMainMenu;
	
	public UI_TableData()
	{
//		setLayout(new GridLayout(2, 1));
		setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		setLocation(500, 300);
		setSize(500, 300);
		
		String queryNames[] = {"--Select query--", 
							"Today Review (Query 5)", 
							"No Rate Review (Query 6)", 
							"High Rate Review (Query 7)",
							"Top Feature (Query 8)", 
							"Misleading Member (Query 9)"};
		
		ddQuery = new JComboBox<String>(queryNames);

		add(ddQuery);
		
		
		results = new JTextArea();
		results.setEditable(false);	
		add(results);

		
//		ShowAllCar();
//		ShowTodayReview();
//		ShowNoRatedReview();
//		ShowHighRatedReview();
//		ShowTopFeature();
//		ShowMisleadMember();
		
		btnMainMenu = new JButton("Main Menu");
		add(btnMainMenu);
		
		UserHandler handler = new UserHandler();
		ddQuery.addActionListener(handler);
		btnMainMenu.addActionListener(handler);
	}
	
//	private void ShowAllCar()
//	{
//		DB_Car db_car = new DB_Car();
//		ShowData(db_car.GetAllCar());
//	}
	
	private void ShowTodayReview()
	{
		DB_Query db_query = new DB_Query();
//		ShowData(db_query.GetTodayReview());
		ShowData(db_query.GetProcedureResult("select_today_review"));
	}
	
	private void ShowNoRatedReview()
	{
		DB_Query db_query = new DB_Query();
		ShowData(db_query.GetProcedureResult("select_no_rate_review"));
	}
	
	private void ShowHighRatedReview()
	{
		DB_Query db_query = new DB_Query();
		ShowData(db_query.GetProcedureResult("select_high_review"));
	}
	
	private void ShowTopFeature()
	{
		DB_Query db_query = new DB_Query();
		ShowData(db_query.GetProcedureResult("select_top_feature"));
	}
	
	private void ShowMisleadMember()
	{
		DB_Query db_query = new DB_Query();
		ShowData(db_query.GetProcedureResult("select_misleading_member"));
	}
	
	
	
	public void ShowData(ResultSet r)
	{
		try{
			ResultSetMetaData meta = r.getMetaData();
			
			String output = "";
			
			for(int i=1; i <= meta.getColumnCount(); i++)
			{
				output += meta.getColumnLabel(i);
				output += "\t";
			}
			
			output += "\n";
			
			while(r.next())
			{
				for(int i=1; i <= meta.getColumnCount(); i++)
				{
					output += r.getString(i);
					output += "\t";
//					String colName = meta.getColumnLabel(i);
//					if(meta.getColumnLabel(i).length() > 10)
//					{
//						// add extra tab
//						output += "\t";
//					}
				}
				output += "\n";
			}
			
			results.setText(output);
			
			r.close();
		}
		catch(Exception ex)
		{
			System.out.println("error" + ex);
		}
	}
    
	private class UserHandler implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event) {
			// TODO Auto-generated method stub
			
			if(event.getSource() == ddQuery && ddQuery.getSelectedIndex() > 0)
			{
				switch(ddQuery.getSelectedIndex())
				{
				case 1:
					ShowTodayReview();
					break;
				
				case 2:
					ShowNoRatedReview();
					break;
					
				case 3:
					ShowHighRatedReview();
					break;
					
				case 4:
					ShowTopFeature();
					break;
					
				case 5:
					ShowMisleadMember();
					break;
				
				}
			}
			
			else if(event.getSource() == btnMainMenu)
			{
				// goto main menu
				UI_Main_Menu mainMenu = new UI_Main_Menu();
				mainMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				mainMenu.setVisible(true);
				dispose();
			}
		}
		
	}
	
//    public static void main(String[] args) 
//    {
//        UI_TableData table = new UI_TableData();
//        table.setVisible(true);
//        table.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//    }

}
